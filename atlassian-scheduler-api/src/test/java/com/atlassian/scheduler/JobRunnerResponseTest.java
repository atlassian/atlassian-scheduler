package com.atlassian.scheduler;

import java.util.concurrent.ExecutionException;

import org.junit.Test;

import com.atlassian.scheduler.status.RunDetails;
import com.atlassian.scheduler.status.RunOutcome;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import static com.atlassian.scheduler.JobRunnerResponse.aborted;
import static com.atlassian.scheduler.JobRunnerResponse.failed;
import static com.atlassian.scheduler.JobRunnerResponse.success;
import static com.atlassian.scheduler.status.RunOutcome.ABORTED;
import static com.atlassian.scheduler.status.RunOutcome.FAILED;
import static com.atlassian.scheduler.status.RunOutcome.SUCCESS;

/**
 * @since v1.0
 */
@SuppressWarnings("ConstantConditions")
public class JobRunnerResponseTest {
    @Test
    public void testSuccessNoArg() {
        assertResponse(success(), SUCCESS, null);
    }

    @Test
    public void testSuccessMessageNull() {
        assertResponse(success(null), SUCCESS, null);
    }

    @Test
    public void testSuccessMessage() {
        assertResponse(success("Info"), SUCCESS, "Info");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testIgnoredMessageNull() {
        aborted(null);
    }

    @Test
    public void testIgnoredMessage() {
        assertResponse(aborted("Info"), ABORTED, "Info");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFailedMessageNull() {
        failed((String) null);
    }

    @Test
    public void testFailedMessage() {
        assertResponse(failed("Info"), FAILED, "Info");
    }

    @Test(expected = NullPointerException.class)
    public void testFailedExceptionNull() {
        failed((Exception) null);
    }

    @Test
    public void testFailedExceptionNoChain() {
        final NullPointerException npe = new NullPointerException();
        assertResponse(failed(npe), FAILED, "NullPointerException");
    }

    @Test
    public void testFailedExceptionShortChain() {
        final NullPointerException npe = new NullPointerException();
        final ExecutionException ex = new ExecutionException(npe);
        assertResponse(failed(ex), FAILED, "ExecutionException: java.lang.NullPointerException\nNullPointerException");
    }

    @Test
    public void testFailedExceptionLongChain() {
        Throwable e = new IllegalArgumentException("The root of all evil");
        e = new ExecutionException(e.toString(), e);
        e = new ExecutionException(e.toString(), e);
        e = new ExecutionException(e.toString(), e);

        final JobRunnerResponse response = failed(e);
        assertThat(response.getRunOutcome(), equalTo(FAILED));
        assertThat(response.getMessage(), containsString("ExecutionException"));
        assertThat(response.getMessage(), containsString("IllegalArgumentException"));
        assertThat(response.getMessage(), containsString("root"));
        assertThat(response.getMessage().length(), equalTo(RunDetails.MAXIMUM_MESSAGE_LENGTH));
    }

    private static void assertResponse(JobRunnerResponse response, RunOutcome expectedOutcome, String expectedMessage) {
        assertThat(response.getRunOutcome(), equalTo(expectedOutcome));
        assertThat(response.getMessage(), equalTo(expectedMessage));
    }
}
