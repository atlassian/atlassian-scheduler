package com.atlassian.scheduler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.annotations.PublicSpi;
import com.atlassian.scheduler.config.JobRunnerKey;

/**
 * Invoked by the {@link SchedulerService} when it is time for a scheduled job to run.
 * <p>
 * Application code should register the JobRunner on startup, and need do nothing on shutdown.
 * </p><p>
 * Plugins should register the JobRunner implementation at startup/plugin enabled, and
 * {@link SchedulerService#unregisterJobRunner(JobRunnerKey) unregister} the {@code JobRunner}
 * when the plugin is disabled.
 * </p>
 */
@PublicSpi
public interface JobRunner {
    /**
     * Called by the {@link SchedulerService} when it is time for a job to run.
     * The job is expected to perform its own error handling by catching exceptions
     * as appropriate and reporting an informative message using
     * {@link JobRunnerResponse#failed(String)}.
     *
     * @param request the information about the request that was supplied by the scheduler service
     * @return a {@link JobRunnerResponse} providing additional detail about the result of running the job.
     * The response is permitted to be {@code null}, which is treated as identical to
     * {@link JobRunnerResponse#success()}.
     */
    @Nullable
    JobRunnerResponse runJob(JobRunnerRequest request);

    /**
     * Returns a class loader suitable for deserializing the parameters map provided by the {@link JobRunnerRequest}.
     * <p>
     * For a class to be loaded successfully, it must be present in the classpath of the initiating class loader
     * (the class loader returned by this method) or any of its parent class loaders.
     * <p>
     * For example, if the parameters map in {@link #runJob(JobRunnerRequest)} contains a custom class
     * {@code CustomClass}, this {@code CustomClass} must be accessible from the JobRunner's class loader
     * (returned by this method) for successful deserialization of the parameters map.
     * <p>
     * The default implementation returns the class loader of the {@link JobRunner} implementation class.
     *
     * @return the class loader to use when deserializing the parameters map
     */
    @Nonnull
    default ClassLoader getParametersClassLoader() {
        return getClass().getClassLoader();
    }
}
