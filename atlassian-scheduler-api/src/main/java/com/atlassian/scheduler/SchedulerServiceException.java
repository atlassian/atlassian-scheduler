package com.atlassian.scheduler;

import com.atlassian.annotations.PublicApi;

/**
 * Indicates a problem interacting with the {@code SchedulerService}.
 * This is a checked exception and is used to report immediate failures,
 * such as failed job registrations.
 *
 * @see SchedulerRuntimeException
 * @since v1.0
 */
@PublicApi
public class SchedulerServiceException extends Exception {
    private static final long serialVersionUID = 1L;

    public SchedulerServiceException(final String message) {
        super(message);
    }

    public SchedulerServiceException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
