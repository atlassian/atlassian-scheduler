package com.atlassian.scheduler;

import com.atlassian.annotations.PublicApi;

/**
 * Indicates a problem interacting with the {@code SchedulerService}.
 * This is an unchecked exception and is used to report unexpected inconsistencies
 * in the scheduler configuration.
 *
 * @see SchedulerServiceException
 * @since v1.0
 */
@PublicApi
public class SchedulerRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public SchedulerRuntimeException(final String message) {
        super(message);
    }

    public SchedulerRuntimeException(final String message, final Throwable cause) {
        super(message, cause);
    }
}
