/**
 * Utility classes for internal use by the API.
 */
@Internal
@ParametersAreNonnullByDefault
package com.atlassian.scheduler.util;

import javax.annotation.ParametersAreNonnullByDefault;

import com.atlassian.annotations.Internal;
