package com.atlassian.scheduler.config;

import java.util.Date;
import java.util.Objects;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import com.atlassian.annotations.PublicApi;

import static com.google.common.base.Preconditions.checkArgument;

import static com.atlassian.scheduler.util.Safe.copy;

/**
 * The description of an {@link Schedule#forInterval(long, Date)} interval schedule}.
 *
 * @since v1.0
 */
@Immutable
@PublicApi
public final class IntervalScheduleInfo {
    private final Date firstRunTime;
    private final long intervalInMillis;

    IntervalScheduleInfo(@Nullable final Date firstRunTime, final long intervalInMillis) {
        this.firstRunTime = copy(firstRunTime);
        this.intervalInMillis = intervalInMillis;

        checkArgument(intervalInMillis >= 0L, "intervalInMillis must not be negative");
    }

    @Nullable
    public Date getFirstRunTime() {
        return copy(firstRunTime);
    }

    public long getIntervalInMillis() {
        return intervalInMillis;
    }

    @Override
    public boolean equals(@Nullable final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final IntervalScheduleInfo other = (IntervalScheduleInfo) o;
        return intervalInMillis == other.intervalInMillis && Objects.equals(firstRunTime, other.firstRunTime);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstRunTime, intervalInMillis);
    }

    @Override
    public String toString() {
        return "IntervalScheduleInfo[firstRunTime=" + firstRunTime + ",intervalInMillis=" + intervalInMillis + ']';
    }
}
