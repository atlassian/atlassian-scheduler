/**
 * Major component interfaces and supporting classes for the atlassian-scheduler API.
 */
@ParametersAreNonnullByDefault
package com.atlassian.scheduler;

import javax.annotation.ParametersAreNonnullByDefault;
