## Overview ##

The `atlassian-scheduler` library is an API for creating scheduled tasks in
Atlassian applications.  See the `SchedulerService` class to get started.
If your target application is not in the list below or you need to support
older versions, then you will need to use the `atlassian-scheduler-compat`
library.

## Ownership

This project is owned by the Server Java Platform team (go/abracadabra).

## Minimum supported product versions: ##

* Confluence v5.10
* Crowd v2.8.0
* JIRA v6.3
* Bitbucket Server v5.0

## Builds ##

This project requires Maven 3.

* Issue tracking: https://jira.atlassian.com/browse/SCHEDULER
* Builds (Internal): https://ecosystem-bamboo.internal.atlassian.com/browse/SCHED

## Copyright ##

Copyright @ 2002 - 2015 Atlassian Pty Ltd

Licensed under the Apache License, Version 2.0 (the "License"); you may not use
this file except in compliance with the License. You may obtain a copy of the
License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software distributed
under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
CONDITIONS OF ANY KIND, either express or implied. See the License for the
specific language governing permissions and limitations under the License.

