# Public

## Moving types (method signatures) around

We introduced replacements for existing API. This has the following advantages:
 - Third party dependency `org.joda.time` is replaced by `java.time` APIs.

| Class name                                                 | Code                                             | Alternative code                           |
|------------------------------------------------------------|--------------------------------------------------|--------------------------------------------|
| com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate | DateTimeTemplate(final ReadableDateTime)         | DateTimeTemplate(final ZonedDateTime)      |
| com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate | DateTimeTemplate(final Date, final DateTimeZone) | DateTimeTemplate(final Date, final ZoneId) |
| com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate | DateTimeZone getZone()                           | ZoneId getZoneId()                         |
| com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate | org.joda.time.LocalDate toFirstOfMonth()         | java.time.LocalDate toFirstDayOfMonth()    |
| com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate | DateTime toDateTime()                            | ZonedDateTime toZonedDateTime()            |
| com.atlassian.scheduler.core.tests.CronFactory             | parse(String, DateTimeZone)                      | parse(String, ZoneId)                      |
| com.atlassian.scheduler.quartz2.Quartz2CronFactory         | parse(String, DateTimeZone)                      | parse(String, ZoneId)                      |
