package com.atlassian.scheduler.core.tests;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.hamcrest.Matcher;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import static com.atlassian.scheduler.core.tests.CronFactory.CronExpressionAdapter;

/**
 * @since v1.5
 */
public abstract class AbstractCronExpressionTest {
    protected final CronFactory cronFactory;

    protected AbstractCronExpressionTest(final CronFactory cronFactory) {
        this.cronFactory = cronFactory;
    }

    @SuppressWarnings("unchecked")
    protected void assertCron(final String message, final String cronExpression, final Matcher... matchers) {
        assertThat(message, cronFactory.parse(cronExpression), allOf(matchers));
    }

    protected void assertQuartzBug(
            final String whyQuartzIsWrong, final String cronExpression, final Matcher<?> matcher) {
        final CronExpressionAdapter cron = cronFactory.parse(cronExpression);
        if (matcher.matches(cron)) {
            fail("Expected a Quartz bug to cause a mismatch between <" + cronExpression + "> and <" + matcher
                    + "> because " + whyQuartzIsWrong + " -- Maybe this bug has been fixed, now?");
        } else {
            System.err.println("Quartz failed to match <" + cronExpression + "> with <" + matcher
                    + "> due to known bug: " + whyQuartzIsWrong);
        }
    }

    protected void assertRunTimes(
            final String message,
            final String cronExpression,
            final ZonedDateTime startingAfter,
            final Matcher... matchers) {
        final CronExpressionAdapter cron = cronFactory.parse(cronExpression, startingAfter.getZone());
        assertRunTimes(message, cron, startingAfter, matchers);
    }

    protected void assertRunTimes(
            final String message,
            final CronExpressionAdapter cron,
            final ZonedDateTime startingAfter,
            final Matcher... matchers) {
        final List<Date> list = generateNextRunTimes(cron, startingAfter, matchers.length);
        boolean ok = false;
        try {
            assertThat(message, list, contains(matchers));
            ok = true;
        } finally {
            if (!ok) {
                final ZoneId zone = startingAfter.getZone();
                System.err.println("Debug info for run times of '" + cron + "':");
                System.err.println("\t" + startingAfter.toInstant().toEpochMilli() + " = " + startingAfter
                        + " (T = starting point)");
                for (Date date : list) {
                    System.err.println(
                            "\t" + date.getTime() + " = " + date.toInstant().atZone(zone) + " (T + "
                                    + MILLISECONDS.toMinutes(date.getTime()
                                            - startingAfter.toInstant().toEpochMilli()) + " min)");
                }
            }
        }
    }

    protected List<Date> generateNextRunTimes(
            final CronExpressionAdapter adapter, final ZonedDateTime startingAfter, final int maximumResults) {
        final List<Date> list = new ArrayList<Date>(maximumResults);
        Date current = new Date(startingAfter.toInstant().toEpochMilli());
        for (int i = 0; i < maximumResults && current != null; ++i) {
            current = adapter.nextRunTime(current);
            list.add(current);
        }
        return list;
    }
}
