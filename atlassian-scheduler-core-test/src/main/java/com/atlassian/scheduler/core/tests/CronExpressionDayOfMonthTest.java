package com.atlassian.scheduler.core.tests;

import org.junit.Test;

import static com.atlassian.scheduler.core.tests.CronExpressionAssertions.satisfiedBy;
import static com.atlassian.scheduler.core.tests.CronExpressionAssertions.unsatisfiedBy;

/**
 * Tests covering the functionality of the day-of-month cron field.
 *
 * @since v1.5
 */
public abstract class CronExpressionDayOfMonthTest extends AbstractCronExpressionTest {
    protected CronExpressionDayOfMonthTest(CronFactory cronFactory) {
        super(cronFactory);
    }

    @Test
    public void testLastDayOfMonth() {
        assertCron(
                "L (normal)",
                "0 0 0 L * ?",
                unsatisfiedBy(0, 0, 0, 30, 1, 2014),
                satisfiedBy(0, 0, 0, 31, 1, 2014),
                unsatisfiedBy(0, 0, 0, 1, 2, 2014),
                unsatisfiedBy(0, 0, 0, 1, 3, 2014),
                satisfiedBy(0, 0, 0, 31, 3, 2014),
                satisfiedBy(0, 0, 0, 30, 4, 2014),
                satisfiedBy(0, 0, 0, 30, 9, 2014),
                satisfiedBy(0, 0, 0, 31, 12, 2014));

        assertCron(
                "L (February)",
                "0 0 0 L * ?",
                unsatisfiedBy(0, 0, 0, 28, 2, 2000),
                satisfiedBy(0, 0, 0, 29, 2, 2000),
                unsatisfiedBy(0, 0, 0, 27, 2, 2014),
                satisfiedBy(0, 0, 0, 28, 2, 2014),
                unsatisfiedBy(0, 0, 0, 28, 2, 2016),
                satisfiedBy(0, 0, 0, 29, 2, 2016));
    }

    @Test
    public void testLastWeekdayOfMonth() {
        assertCron(
                "LW (normal)",
                "0 0 0 LW * ?",
                unsatisfiedBy(0, 0, 0, 30, 1, 2014),
                satisfiedBy(0, 0, 0, 31, 1, 2014),
                unsatisfiedBy(0, 0, 0, 30, 3, 2014),
                satisfiedBy(0, 0, 0, 31, 3, 2014),
                unsatisfiedBy(0, 0, 0, 1, 2, 2014),
                unsatisfiedBy(0, 0, 0, 30, 3, 2014),
                satisfiedBy(0, 0, 0, 31, 3, 2014),
                unsatisfiedBy(0, 0, 0, 28, 8, 2014),
                satisfiedBy(0, 0, 0, 29, 8, 2014),
                unsatisfiedBy(0, 0, 0, 30, 8, 2014),
                unsatisfiedBy(0, 0, 0, 31, 8, 2014),
                satisfiedBy(0, 0, 0, 28, 11, 2014));

        assertCron(
                "LW (February)",
                "0 0 0 LW * ?",
                unsatisfiedBy(0, 0, 0, 28, 2, 2000),
                satisfiedBy(0, 0, 0, 29, 2, 2000),
                satisfiedBy(0, 0, 0, 27, 2, 2004),
                unsatisfiedBy(0, 0, 0, 28, 2, 2004),
                unsatisfiedBy(0, 0, 0, 26, 2, 2016),
                unsatisfiedBy(0, 0, 0, 27, 2, 2016),
                unsatisfiedBy(0, 0, 0, 28, 2, 2016),
                satisfiedBy(0, 0, 0, 29, 2, 2016));
    }

    @Test
    public void testClosestWeekdayToDayOfMonth() {
        assertCron(
                "7W",
                "0 0 0 7W * ?",
                unsatisfiedBy(0, 0, 0, 6, 1, 2014), // January
                satisfiedBy(0, 0, 0, 7, 1, 2014),
                unsatisfiedBy(0, 0, 0, 8, 1, 2014),
                unsatisfiedBy(0, 0, 0, 6, 2, 2014), // February
                satisfiedBy(0, 0, 0, 7, 2, 2014),
                unsatisfiedBy(0, 0, 0, 8, 2, 2014),
                unsatisfiedBy(0, 0, 0, 5, 6, 2014), // June (7th on Saturday)
                satisfiedBy(0, 0, 0, 6, 6, 2014),
                unsatisfiedBy(0, 0, 0, 7, 6, 2014),
                unsatisfiedBy(0, 0, 0, 8, 6, 2014),
                unsatisfiedBy(0, 0, 0, 9, 6, 2014),
                unsatisfiedBy(0, 0, 0, 5, 9, 2014), // September (7th on Sunday)
                unsatisfiedBy(0, 0, 0, 6, 9, 2014),
                unsatisfiedBy(0, 0, 0, 7, 9, 2014),
                satisfiedBy(0, 0, 0, 8, 9, 2014),
                unsatisfiedBy(0, 0, 0, 9, 9, 2014));

        assertCron(
                "28W",
                "0 0 0 28W * ?",
                satisfiedBy(0, 0, 0, 27, 2, 2004),
                unsatisfiedBy(0, 0, 0, 28, 2, 2004),
                unsatisfiedBy(0, 0, 0, 29, 2, 2004),
                unsatisfiedBy(0, 0, 0, 26, 2, 2015),
                satisfiedBy(0, 0, 0, 27, 2, 2015),
                unsatisfiedBy(0, 0, 0, 28, 2, 2015),
                unsatisfiedBy(0, 0, 0, 26, 2, 2016),
                unsatisfiedBy(0, 0, 0, 27, 2, 2016),
                unsatisfiedBy(0, 0, 0, 28, 2, 2016),
                satisfiedBy(0, 0, 0, 29, 2, 2016));

        assertCron(
                "29W",
                "0 0 0 29W * ?",
                satisfiedBy(0, 0, 0, 27, 2, 2004),
                unsatisfiedBy(0, 0, 0, 28, 2, 2004),
                unsatisfiedBy(0, 0, 0, 29, 2, 2004),
                unsatisfiedBy(0, 0, 0, 27, 2, 2014),
                // unsatisfiedBy(0, 0, 0, 28, 2, 2014),  // See testNearestWeekdayToLastDayOfMonthEdgeCase
                unsatisfiedBy(0, 0, 0, 1, 3, 2014),
                unsatisfiedBy(0, 0, 0, 26, 2, 2015),
                unsatisfiedBy(0, 0, 0, 27, 2, 2015),
                unsatisfiedBy(0, 0, 0, 28, 2, 2015),
                unsatisfiedBy(0, 0, 0, 26, 2, 2016),
                unsatisfiedBy(0, 0, 0, 27, 2, 2016),
                unsatisfiedBy(0, 0, 0, 28, 2, 2016),
                satisfiedBy(0, 0, 0, 29, 2, 2016));
    }

    /**
     * Quartz fails the test for this edge case.
     * <p>
     * Given:
     * </p>
     * <ul>
     * <li>The day-of-month rule given is of the form {@code nW}, where {@code n} is {@code 29}, {@code 30}, or
     * {@code 31}; and</li>
     * <li>A day under consideration falls in a month with {@code n-1} days; and</li>
     * <li>The last day of the month is a Friday.</li>
     * </ul>
     * The correct answer should be that it does not match.  For example, for the rule {@code "0 0 0 29W * ?"},
     * {@code Fri 2014-02-28} should not match, because {@code 2014-02-29} does not exist.  Quartz, however,
     * calls this a match.  It says the same thing about {@code Fri 2010/04/30} for the {@code 31W} case.
     */
    @Test
    public void testNearestWeekdayToLastDayOfMonthEdgeCase() {
        assertCron("Edge case - 2014/28/02 and 29W", "0 0 0 29W * ?", unsatisfiedBy(0, 0, 0, 28, 2, 2014));
        assertCron("Edge case - 2010/04/30 and 31W", "0 0 0 31W * ?", unsatisfiedBy(0, 0, 0, 30, 4, 2010));
    }

    @Test
    public void testClosestWeekdayToOffsetFromLastDayOfMonth() {
        assertCron(
                "L-3W (normal)",
                "0 0 0 L-3W * ?",
                unsatisfiedBy(0, 0, 0, 27, 1, 2014),
                satisfiedBy(0, 0, 0, 28, 1, 2014),
                unsatisfiedBy(0, 0, 0, 29, 1, 2014),
                unsatisfiedBy(0, 0, 0, 30, 1, 2014),
                unsatisfiedBy(0, 0, 0, 31, 1, 2014),
                unsatisfiedBy(0, 0, 0, 1, 2, 2014),
                satisfiedBy(0, 0, 0, 28, 3, 2014),
                unsatisfiedBy(0, 0, 0, 27, 4, 2014),
                satisfiedBy(0, 0, 0, 28, 4, 2014),
                satisfiedBy(0, 0, 0, 26, 9, 2014),
                unsatisfiedBy(0, 0, 0, 27, 9, 2014),
                unsatisfiedBy(0, 0, 0, 28, 12, 2014),
                satisfiedBy(0, 0, 0, 29, 12, 2014));

        assertCron(
                "L-3W (February)",
                "0 0 0 L-3W * ?",
                unsatisfiedBy(0, 0, 0, 24, 2, 2000),
                satisfiedBy(0, 0, 0, 25, 2, 2000),
                unsatisfiedBy(0, 0, 0, 26, 2, 2000),
                unsatisfiedBy(0, 0, 0, 26, 2, 2012),
                satisfiedBy(0, 0, 0, 27, 2, 2012),
                unsatisfiedBy(0, 0, 0, 28, 2, 2012),
                unsatisfiedBy(0, 0, 0, 24, 2, 2014),
                satisfiedBy(0, 0, 0, 25, 2, 2014),
                unsatisfiedBy(0, 0, 0, 26, 2, 2014),
                unsatisfiedBy(0, 0, 0, 27, 2, 2014),
                unsatisfiedBy(0, 0, 0, 25, 2, 2016),
                satisfiedBy(0, 0, 0, 26, 2, 2016),
                unsatisfiedBy(0, 0, 0, 27, 2, 2016));
    }

    @Test
    public void testOffsetFromLastDayOfMonth() {
        assertCron(
                "L-3 (normal)",
                "0 0 0 L-3 * ?",
                unsatisfiedBy(0, 0, 0, 27, 1, 2014),
                satisfiedBy(0, 0, 0, 28, 1, 2014),
                unsatisfiedBy(0, 0, 0, 29, 1, 2014),
                unsatisfiedBy(0, 0, 0, 30, 1, 2014),
                unsatisfiedBy(0, 0, 0, 31, 1, 2014),
                unsatisfiedBy(0, 0, 0, 1, 2, 2014),
                satisfiedBy(0, 0, 0, 28, 3, 2014),
                satisfiedBy(0, 0, 0, 27, 4, 2014),
                satisfiedBy(0, 0, 0, 27, 9, 2014),
                satisfiedBy(0, 0, 0, 28, 12, 2014));

        assertCron(
                "L-3 (February)",
                "0 0 0 L-3 * ?",
                unsatisfiedBy(0, 0, 0, 25, 2, 2000),
                satisfiedBy(0, 0, 0, 26, 2, 2000),
                unsatisfiedBy(0, 0, 0, 27, 2, 2000),
                unsatisfiedBy(0, 0, 0, 28, 2, 2000),
                unsatisfiedBy(0, 0, 0, 29, 2, 2000),
                unsatisfiedBy(0, 0, 0, 24, 2, 2014),
                satisfiedBy(0, 0, 0, 25, 2, 2014),
                unsatisfiedBy(0, 0, 0, 26, 2, 2014),
                unsatisfiedBy(0, 0, 0, 25, 2, 2016),
                satisfiedBy(0, 0, 0, 26, 2, 2016),
                unsatisfiedBy(0, 0, 0, 27, 2, 2016));
    }
}
