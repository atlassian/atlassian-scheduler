package com.atlassian.scheduler.core.tests;

import java.time.ZoneId;
import java.util.Date;
import javax.annotation.Nullable;

/**
 * Generate cron expression facades for a test.
 *
 * @since v1.5
 */
public interface CronFactory {
    void parseAndDiscard(String cronExpression);

    CronExpressionAdapter parse(String cronExpression);

    CronExpressionAdapter parse(String cronExpression, ZoneId zone);

    interface CronExpressionAdapter {
        boolean isSatisfiedBy(final Date date);

        @Nullable
        Date nextRunTime(final Date dateTime);
    }
}
