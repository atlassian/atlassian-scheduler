package com.atlassian.scheduler.core.tests;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import com.atlassian.scheduler.core.tests.CronFactory.CronExpressionAdapter;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;

/**
 * Cron expression to date matcher.
 * <p>
 * Note that the order of the arguments matches that of the cron expressions so that it is
 * easier to tell by inspection whether or not the expression should match.
 * </p>
 *
 * @since v1.5
 */
public class CronExpressionAssertions {
    public static Matcher<CronExpressionAdapter> satisfiedBy(
            final int second, final int minute, final int hour, final int day, final int month, final int year) {
        return satisfiedBy(second, minute, hour, day, month, year, ZoneId.systemDefault());
    }

    public static Matcher<CronExpressionAdapter> satisfiedBy(
            final int second,
            final int minute,
            final int hour,
            final int day,
            final int month,
            final int year,
            final ZoneId zone) {
        return new TypeSafeMatcher<CronExpressionAdapter>() {
            @SuppressWarnings("MagicConstant")
            @Override
            protected boolean matchesSafely(final CronExpressionAdapter cronExpression) {
                final ZonedDateTime dateTime = ZonedDateTime.of(year, month, day, hour, minute, second, 0, zone);
                return cronExpression.isSatisfiedBy(Date.from(dateTime.toInstant()));
            }

            @Override
            public void describeTo(final Description description) {
                description
                        .appendText("(")
                        .appendText(s(second))
                        .appendText(", ")
                        .appendText(s(minute))
                        .appendText(", ")
                        .appendText(s(hour))
                        .appendText(", ")
                        .appendText(s(day))
                        .appendText(", ")
                        .appendText(s(month))
                        .appendText(", ")
                        .appendText(s(year))
                        .appendText(", ")
                        .appendText(zone.getId())
                        .appendText(")");
            }
        };
    }

    public static Matcher<CronExpressionAdapter> unsatisfiedBy(
            int second, int minute, int hour, int day, int month, int year) {
        return not(satisfiedBy(second, minute, hour, day, month, year));
    }

    public static Matcher<CronExpressionAdapter> unsatisfiedBy(
            int second, int minute, int hour, int day, int month, int year, ZoneId zone) {
        return not(satisfiedBy(second, minute, hour, day, month, year, zone));
    }

    public static Matcher<? super Date> runTime(final long timestamp) {
        return new TypeSafeMatcher<Date>() {
            @Override
            protected boolean matchesSafely(Date item) {
                return item.getTime() == timestamp;
            }

            @Override
            public void describeTo(Description description) {
                description
                        .appendText("<Date with timestamp=")
                        .appendText(String.valueOf(timestamp))
                        .appendText("=")
                        .appendText(new Date(timestamp).toString())
                        .appendText(">");
            }

            @Override
            protected void describeMismatchSafely(Date item, Description mismatchDescription) {
                mismatchDescription
                        .appendText("<Date with timestamp=")
                        .appendText(String.valueOf(item.getTime()))
                        .appendText("=")
                        .appendText(item.toString())
                        .appendText(">");
            }
        };
    }

    public static Matcher<Date> runTime(
            final int second,
            final int minute,
            final int hour,
            final int day,
            final int month,
            final int year,
            final ZoneId zone) {
        return new TypeSafeMatcher<Date>() {
            @Override
            protected boolean matchesSafely(final Date result) {
                final ZonedDateTime dateTime = result.toInstant().atZone(zone);
                return dateTime.getYear() == year
                        && dateTime.getMonthValue() == month
                        && dateTime.getDayOfMonth() == day
                        && dateTime.getHour() == hour
                        && dateTime.getMinute() == minute
                        && dateTime.getSecond() == second;
            }

            @Override
            public void describeTo(Description description) {
                description
                        .appendText("(")
                        .appendText(s(second))
                        .appendText(", ")
                        .appendText(s(minute))
                        .appendText(", ")
                        .appendText(s(hour))
                        .appendText(", ")
                        .appendText(s(day))
                        .appendText(", ")
                        .appendText(s(month))
                        .appendText(", ")
                        .appendText(s(year))
                        .appendText(", ")
                        .appendText(zone.getId())
                        .appendText(")");
            }

            @Override
            protected void describeMismatchSafely(Date item, Description mismatchDescription) {
                final ZonedDateTime dateTime = item.toInstant().atZone(zone);
                mismatchDescription
                        .appendText("(")
                        .appendText(s(dateTime.getSecond()))
                        .appendText(", ")
                        .appendText(s(dateTime.getMinute()))
                        .appendText(", ")
                        .appendText(s(dateTime.getHour()))
                        .appendText(", ")
                        .appendText(s(dateTime.getDayOfMonth()))
                        .appendText(", ")
                        .appendText(s(dateTime.getMonthValue()))
                        .appendText(", ")
                        .appendText(s(dateTime.getYear()))
                        .appendText(", ")
                        .appendText(zone.getId())
                        .appendText(")");
            }
        };
    }

    public static Matcher<Date> runTime(
            final int second,
            final int minute,
            final int hour,
            final int day,
            final int month,
            final int year,
            final ZoneId zone,
            final long millis) {
        //noinspection unchecked
        return allOf(runTime(millis), runTime(second, minute, hour, day, month, year, zone));
    }

    static String s(int value) {
        return String.valueOf(value);
    }
}
