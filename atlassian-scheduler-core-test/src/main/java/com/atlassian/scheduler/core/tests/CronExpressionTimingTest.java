package com.atlassian.scheduler.core.tests;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.ref.WeakReference;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

import org.junit.Test;

import static java.lang.System.nanoTime;
import static org.junit.Assert.fail;

/**
 * Maybe be used to collect simple benchmarking results for cron expression evaluation.
 *
 * @since v1.5
 */
public abstract class CronExpressionTimingTest extends AbstractCronExpressionTest {
    private static final Object TEST_LOCK = new Object();
    private static final ZonedDateTime DTZ1 = dtz(2014, 4, 6, 1, 57, 0, 0, "Australia/Sydney");
    private static final ZonedDateTime DTZ2 = dtz(2014, 4, 5, 23, 30, 0, 0, "Australia/Lord_Howe");
    private static final ZonedDateTime DTZ3 = dtz(1970, 2, 1, 0, 0, 0, 0, "Australia/Sydney");

    private static final int PARSE_ITERS = 100000;
    private static final int EVAL_ITERS = 1000;
    private static final int EVAL_REPS = 100;

    private final StringWriter sw = new StringWriter();
    private final PrintWriter pw = new PrintWriter(sw);

    protected CronExpressionTimingTest(CronFactory cronFactory) {
        super(cronFactory);
    }

    @Test
    public void test() {
        // If you run "CronExpressionTimingTest" directly from an IDE, it will generally try
        // to run them in parallel.  That doesn't make any sense to do, so explicitly block it
        // from working.
        synchronized (TEST_LOCK) {
            runTimingTest();
        }
    }

    protected void runTimingTest() {
        pw.println(getClass().getSimpleName() + ':');
        runParseTest("simple", "0 * * ? * *");
        runParseTest("names", "0 * * ? JAN,FEB,MAR,APR,MAY,JUN,JUL,AUG,SEP,OCT,NOV,DEC MON,TUE,WED,THU,FRI,SAT,SUN");
        runParseTest("complex", "* 7-12,57-2 */3 20-3/7 JUN-OCT/3,DEC-APR/2 ? 1970-2099/4");
        runEvalTest("Sydney hours", "0 30 * * * ?", DTZ1);
        runEvalTest("Lord Howe hours", "0 30 * * * ?", DTZ2);
        runEvalTest("Sydney daily", "0 23 2 * * ?", DTZ1);
        runEvalTest("Sydney twice monthly", "0 30 4 1,15 * ?", DTZ1);
        runEvalTest("Sydney evil", "1 */17 */23 31 2,4,6,9,11,12 ? */4", DTZ3);
        pw.flush();
        System.err.println(sw.toString());
        sw.getBuffer().setLength(0);
    }

    protected void runParseTest(final String desc, final String cronExpression) {
        runParseTest(desc, cronExpression, PARSE_ITERS);
    }

    protected void runParseTest(final String desc, final String cronExpression, final int iters) {
        cleanMem();
        final long startTime = nanoTime();
        for (int i = 0; i < iters; ++i) {
            cronFactory.parseAndDiscard(cronExpression);
        }
        final long time = nanoTime() - startTime;
        report("runParse", desc, iters, time);
    }

    protected void runEvalTest(final String desc, final String cronExpression, final ZonedDateTime startingAt) {
        runEvalTest(desc, cronExpression, EVAL_ITERS, startingAt, EVAL_REPS);
    }

    protected void runEvalTest(
            final String desc,
            final String cronExpression,
            final int iters,
            final ZonedDateTime startingAt,
            final int reps) {
        final Date startingDate = Date.from(startingAt.toInstant());
        final CronFactory.CronExpressionAdapter cron = cronFactory.parse(cronExpression, startingAt.getZone());

        cleanMem();
        final long startTime = nanoTime();
        for (int i = 0; i < iters; ++i) {
            Date date = startingDate;
            for (int rep = 0; rep < reps; ++rep) {
                date = cron.nextRunTime(date);
            }
        }
        final long time = nanoTime() - startTime;
        report("runEvalTest", desc, iters, time);
    }

    private void report(final String what, final String desc, final int iters, final long time) {
        pw.println("\t" + what + ": " + desc + "; iters=" + iters + "; time=" + time + "ns; iters/sec="
                + (long) (((double) iters / (double) time) * 1000000000L));
    }

    private static ZonedDateTime dtz(
            final int year,
            final int month,
            final int day,
            final int hour,
            final int minute,
            final int second,
            final int nanoSeconds,
            final String timeZoneId) {
        return ZonedDateTime.of(year, month, day, hour, minute, second, nanoSeconds, ZoneId.of(timeZoneId));
    }

    private static void cleanMem() {
        final WeakReference<Object> ref = new WeakReference<Object>(new Object());
        for (int i = 0; i < 100; ++i) {
            System.gc();
            if (ref.get() == null) {
                return;
            }
            try {
                Thread.sleep(100L);
            } catch (InterruptedException ie) {
                throw new AssertionError(ie);
            }
        }
        fail("Ummm, what's wrong with the garbage collector?!");
    }
}
