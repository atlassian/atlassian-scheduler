package com.atlassian.scheduler.core.tests;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

import org.hamcrest.Matcher;
import org.junit.Test;

import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.Schedule;

import static java.lang.System.currentTimeMillis;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.lessThanOrEqualTo;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import static com.atlassian.scheduler.config.Schedule.forCronExpression;
import static com.atlassian.scheduler.config.Schedule.forInterval;

/**
 * @since v1.6.0
 */
public abstract class CalculateNextRunTimeTest {
    protected abstract SchedulerService getSchedulerService();

    @Test
    public void testRunImmediately() {
        // We use a wide window here because Quartz's clock isn't pluggable and whether it adds the interval to
        // the time or not seems to be a fairly random thing.
        final long now = currentTimeMillis();
        assertNextRunTime(forInterval(60000L, null), between(now - 100L, now + 61000L));
    }

    @Test
    public void testRunInThePast() {
        // wide window again (see above)
        final long now = currentTimeMillis();
        assertNextRunTime(forInterval(60000L, new Date(now - 120000L)), between(now - 100L, now + 61000L));
    }

    @Test
    public void testRunLater() {
        // But here we're using an exact time and there shouldn't be any mismatch to it
        final long now = currentTimeMillis();
        assertNextRunTime(forInterval(60000L, new Date(now + 15000L)), now + 15000L);
    }

    @Test
    public void testCronEveryFiveMinutes() {
        final Date nextRunTime = calculateNextRunTime(forCronExpression("0 */5 * * * ?"));
        assertThat(nextRunTime, not(nullValue()));

        final ZonedDateTime dateTime = nextRunTime.toInstant().atZone(ZoneId.systemDefault());
        if (dateTime.getNano() != 0 || dateTime.getSecond() != 0 || (dateTime.getMinute() % 5) != 0) {
            fail("Should have fallen on a 5 minute boundary: " + dateTime);
        }
    }

    @Test
    public void testFebruaryThirtyFirst() {
        assertNeverRuns(forCronExpression("0 0 2 31 2 ?"));
    }

    @Test
    public void testInvalidCronExpression() {
        assertException(forCronExpression("0 0 2 31 2 *"), "must use '?'");
    }

    Matcher<Date> between(long min, long max) {
        //noinspection unchecked
        return allOf(greaterThanOrEqualTo(new Date(min)), lessThanOrEqualTo(new Date(max)));
    }

    Date calculateNextRunTime(Schedule schedule) {
        try {
            return getSchedulerService().calculateNextRunTime(schedule);
        } catch (SchedulerServiceException sse) {
            final AssertionError err =
                    new AssertionError("Unexpected exception finding first run time for " + schedule);
            err.initCause(sse);
            throw err;
        }
    }

    void assertNextRunTime(Schedule schedule, long value) {
        assertNextRunTime(schedule, equalTo(new Date(value)));
    }

    void assertNextRunTime(Schedule schedule, Matcher<? super Date> matcher) {
        assertThat(calculateNextRunTime(schedule), matcher);
    }

    void assertNeverRuns(Schedule schedule) {
        assertThat(calculateNextRunTime(schedule), nullValue(Date.class));
    }

    void assertException(Schedule schedule, String substring) {
        final SchedulerService schedulerService = getSchedulerService();
        try {
            final Date nextRunTime = schedulerService.calculateNextRunTime(schedule);
            fail("Got first run time <" + nextRunTime + "> when exception containing <" + substring
                    + "> was expected for <" + schedule + '>');
        } catch (SchedulerServiceException sse) {
            assertThat(sse.getMessage(), containsString(substring));
        }
    }
}
