package com.atlassian.scheduler.core;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.hamcrest.Matchers;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableSet;

import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.core.impl.MemoryRunDetailsDao;
import com.atlassian.scheduler.status.JobDetails;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.junit.Assert.assertThat;

/**
 * A test fixture for the {@code AbstractSchedulerService}.
 *
 * @since v1.6.0
 */
class SchedulerServiceFixture extends AbstractSchedulerService {
    private static final long HACK_NANO_STEP = MILLISECONDS.toNanos(30L);

    final AtomicInteger startImplCalls = new AtomicInteger();
    final AtomicInteger standbyImplCalls = new AtomicInteger();
    final AtomicInteger shutdownImplCalls = new AtomicInteger();

    private volatile boolean hackAwaitNanos;

    SchedulerServiceFixture() {
        super(new MemoryRunDetailsDao());
    }

    @Override
    protected void startImpl() {
        startImplCalls.incrementAndGet();
    }

    @Override
    protected void standbyImpl() {
        standbyImplCalls.incrementAndGet();
    }

    @Override
    protected void shutdownImpl() {
        shutdownImplCalls.incrementAndGet();
    }

    @Nonnull
    @Override
    public Set<JobRunnerKey> getJobRunnerKeysForAllScheduledJobs() {
        return ImmutableSet.of();
    }

    @Override
    public void scheduleJob(JobId jobId, JobConfig jobConfig) {}

    @Override
    public void unscheduleJob(JobId jobId) {}

    @Nullable
    @Override
    public Date calculateNextRunTime(Schedule schedule) {
        return null;
    }

    @Override
    public JobDetails getJobDetails(JobId jobId) {
        return null;
    }

    @Nonnull
    @Override
    public List<JobDetails> getJobsByJobRunnerKey(JobRunnerKey jobRunnerKey) {
        return ImmutableList.of();
    }

    @Nonnull
    @Override
    public List<JobDetails> getJobsByJobRunnerKeys(List<JobRunnerKey> jobRunnerKeys) {
        return ImmutableList.of();
    }

    @Override
    long awaitNanos(long nanosLeft) throws InterruptedException {
        if (hackAwaitNanos) {
            return nanosLeft - HACK_NANO_STEP;
        }
        return super.awaitNanos(nanosLeft);
    }

    void hackAwaitNanos() {
        hackAwaitNanos = true;
    }

    void assertState(int expectedStartCount, int expectedStandbyCount, int expectedShutdownCount, State state) {
        assertThat(
                "assertState(start, standby, shutdown, state)",
                Arrays.<Object>asList(
                        startImplCalls.get(), standbyImplCalls.get(), shutdownImplCalls.get(), getState()),
                Matchers.<Object>contains(expectedStartCount, expectedStandbyCount, expectedShutdownCount, state));
    }
}
