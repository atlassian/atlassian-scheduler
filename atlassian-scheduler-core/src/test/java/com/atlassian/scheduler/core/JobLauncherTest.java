package com.atlassian.scheduler.core;

import java.util.Date;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;

import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.core.impl.RunningJobImpl;
import com.atlassian.scheduler.core.status.SimpleJobDetails;
import com.atlassian.scheduler.core.status.UnusableJobDetails;
import com.atlassian.scheduler.status.JobDetails;

import static java.lang.Math.abs;
import static java.lang.System.currentTimeMillis;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import static com.atlassian.scheduler.JobRunnerResponse.failed;
import static com.atlassian.scheduler.JobRunnerResponse.success;
import static com.atlassian.scheduler.config.RunMode.RUN_LOCALLY;
import static com.atlassian.scheduler.config.RunMode.RUN_ONCE_PER_CLUSTER;
import static com.atlassian.scheduler.core.Constants.JOB_ID;
import static com.atlassian.scheduler.core.Constants.KEY;
import static com.atlassian.scheduler.status.RunOutcome.ABORTED;
import static com.atlassian.scheduler.status.RunOutcome.FAILED;
import static com.atlassian.scheduler.status.RunOutcome.SUCCESS;
import static com.atlassian.scheduler.status.RunOutcome.UNAVAILABLE;

/**
 * @since v1.0
 */
@SuppressWarnings({"ResultOfObjectAllocationIgnored", "ConstantConditions"})
public class JobLauncherTest {
    private static final Date NOW = new Date();
    private static final Schedule SCHEDULE = Schedule.forInterval(60000L, null);

    @Test(expected = NullPointerException.class)
    public void testSchedulerServiceNull() {
        new JobLauncher(null, RUN_LOCALLY, new Date(), JOB_ID);
    }

    @Test(expected = NullPointerException.class)
    public void testRunModeNull() {
        new JobLauncher(mock(AbstractSchedulerService.class), null, new Date(), JOB_ID);
    }

    @Test(expected = NullPointerException.class)
    public void testJobIdNull() {
        new JobLauncher(mock(AbstractSchedulerService.class), RUN_LOCALLY, new Date(), null);
    }

    @Test
    public void testLaunchJobDetailsNull() {
        final AbstractSchedulerService schedulerService = mock(AbstractSchedulerService.class);
        final JobLauncher jobLauncher = new JobLauncher(schedulerService, RUN_LOCALLY, null, JOB_ID);
        assertCloseToNow(jobLauncher.firedAt);

        jobLauncher.launch();

        verify(schedulerService).addRunDetails(JOB_ID, jobLauncher.firedAt, ABORTED, "No corresponding job details");
    }

    @Test
    public void testLaunchJobDetailsNotRunnable() {
        final AbstractSchedulerService schedulerService = mock(AbstractSchedulerService.class);
        final JobLauncher jobLauncher = new JobLauncher(schedulerService, RUN_LOCALLY, NOW, JOB_ID);
        when(schedulerService.getJobDetails(JOB_ID)).thenReturn(unusable(null));

        jobLauncher.launch();

        verify(schedulerService).addRunDetails(JOB_ID, NOW, UNAVAILABLE, "Job runner key 'test.key' is not registered");
    }

    @Test
    public void testLaunchJobRunnerVanished() {
        final AbstractSchedulerService schedulerService = mock(AbstractSchedulerService.class);
        final JobLauncher jobLauncher = new JobLauncher(schedulerService, RUN_LOCALLY, NOW, JOB_ID);
        when(schedulerService.getJobDetails(JOB_ID)).thenReturn(details());

        jobLauncher.launch();

        verify(schedulerService).addRunDetails(JOB_ID, NOW, UNAVAILABLE, "Job runner key 'test.key' is not registered");
    }

    @Test
    public void testLaunchRunModeInconsistency() {
        final AbstractSchedulerService schedulerService = mock(AbstractSchedulerService.class);
        final JobRunner jobRunner = mock(JobRunner.class);
        final JobLauncher jobLauncher = new JobLauncher(schedulerService, RUN_ONCE_PER_CLUSTER, NOW, JOB_ID);
        when(schedulerService.getJobDetails(JOB_ID)).thenReturn(details());
        when(schedulerService.getJobRunner(KEY)).thenReturn(jobRunner);

        jobLauncher.launch();

        verify(schedulerService)
                .addRunDetails(
                        JOB_ID,
                        NOW,
                        ABORTED,
                        "Inconsistent run mode: expected 'RUN_LOCALLY' got: 'RUN_ONCE_PER_CLUSTER'");
    }

    @Test
    public void testLaunchJobRunnerWhileAlreadyRunning() throws Exception {
        final AbstractSchedulerService schedulerService = mock(AbstractSchedulerService.class);
        final JobRunner jobRunner = mock(JobRunner.class);
        final RunningJob existing = mock(RunningJob.class);
        final JobLauncher jobLauncher = new JobLauncher(schedulerService, RUN_LOCALLY, NOW, JOB_ID);

        when(schedulerService.getJobDetails(JOB_ID)).thenReturn(details());
        when(schedulerService.getJobRunner(KEY)).thenReturn(jobRunner);
        when(schedulerService.enterJob(eq(JOB_ID), any(RunningJob.class))).thenReturn(existing);

        jobLauncher.launch();

        verify(schedulerService).addRunDetails(JOB_ID, NOW, ABORTED, "Already running");
        verify(schedulerService).enterJob(eq(JOB_ID), any(RunningJob.class));
        verify(schedulerService, never()).leaveJob(eq(JOB_ID), any(RunningJob.class));
        verify(schedulerService, never()).unscheduleJob(JOB_ID);
        verify(schedulerService, never()).preJob();
        verify(schedulerService, never()).postJob();
        verify(jobRunner, never()).runJob(new RunningJobImpl(NOW, JOB_ID, config()));
    }

    @Test
    public void testLaunchJobRunnerThatReturnsNull() throws Exception {
        final AbstractSchedulerService schedulerService = mock(AbstractSchedulerService.class);
        final JobRunner jobRunner = mock(JobRunner.class);
        final JobLauncher jobLauncher = new JobLauncher(schedulerService, RUN_LOCALLY, NOW, JOB_ID);

        when(schedulerService.getJobDetails(JOB_ID)).thenReturn(details());
        when(schedulerService.getJobRunner(KEY)).thenReturn(jobRunner);

        jobLauncher.launch();

        verify(schedulerService).addRunDetails(JOB_ID, NOW, SUCCESS, null);
        verify(schedulerService, never()).unscheduleJob(JOB_ID);
        assertJobLifeCycle(schedulerService, jobRunner, config());
    }

    @Test
    public void testLaunchJobRunnerThatReturnsInfo() throws Exception {
        final AbstractSchedulerService schedulerService = mock(AbstractSchedulerService.class);
        final JobRunner jobRunner = mock(JobRunner.class);
        final JobLauncher jobLauncher = new JobLauncher(schedulerService, RUN_LOCALLY, NOW, JOB_ID);
        final JobRunnerRequest request = new RunningJobImpl(NOW, JOB_ID, config());
        final JobRunnerResponse response = success("Info");

        when(schedulerService.getJobDetails(JOB_ID)).thenReturn(details());
        when(schedulerService.getJobRunner(KEY)).thenReturn(jobRunner);
        when(jobRunner.runJob(request)).thenReturn(response);

        jobLauncher.launch();

        verify(schedulerService).addRunDetails(JOB_ID, NOW, SUCCESS, "Info");
        verify(schedulerService, never()).unscheduleJob(JOB_ID);
        assertJobLifeCycle(schedulerService, jobRunner, config());
    }

    @Test
    public void testLaunchDeletesRunOnceJobOnSuccess() throws Exception {
        final AbstractSchedulerService schedulerService = mock(AbstractSchedulerService.class);
        final JobRunner jobRunner = mock(JobRunner.class);
        final JobLauncher jobLauncher = new JobLauncher(schedulerService, RUN_LOCALLY, NOW, JOB_ID);
        final JobDetails jobDetails =
                new SimpleJobDetails(JOB_ID, KEY, RUN_LOCALLY, Schedule.runOnce(null), null, null, null);
        final JobConfig config = config(jobDetails);
        final JobRunnerRequest request = new RunningJobImpl(NOW, JOB_ID, config);
        final JobRunnerResponse response = success("Info");

        when(schedulerService.getJobDetails(JOB_ID)).thenReturn(jobDetails);
        when(schedulerService.getJobRunner(KEY)).thenReturn(jobRunner);
        when(jobRunner.runJob(request)).thenReturn(response);

        jobLauncher.launch();

        verify(schedulerService).addRunDetails(JOB_ID, NOW, SUCCESS, "Info");
        verify(schedulerService).unscheduleJob(JOB_ID);
        assertJobLifeCycle(schedulerService, jobRunner, config);
    }

    @Test
    public void testLaunchDeletesRunOnceJobOnFailure() throws Exception {
        final AbstractSchedulerService schedulerService = mock(AbstractSchedulerService.class);
        final JobRunner jobRunner = mock(JobRunner.class);
        final JobLauncher jobLauncher = new JobLauncher(schedulerService, RUN_LOCALLY, NOW, JOB_ID);
        final JobDetails jobDetails =
                new SimpleJobDetails(JOB_ID, KEY, RUN_LOCALLY, Schedule.runOnce(null), null, null, null);
        final JobConfig config = config(jobDetails);
        final JobRunnerRequest request = new RunningJobImpl(NOW, JOB_ID, config);
        final IllegalArgumentException testEx = new IllegalArgumentException("Just testing!");

        when(schedulerService.getJobDetails(JOB_ID)).thenReturn(jobDetails);
        when(schedulerService.getJobRunner(KEY)).thenReturn(jobRunner);
        when(jobRunner.runJob(request)).thenThrow(testEx);

        jobLauncher.launch();

        verify(schedulerService)
                .addRunDetails(JOB_ID, NOW, FAILED, failed(testEx).getMessage());
        verify(schedulerService).unscheduleJob(JOB_ID);
        assertJobLifeCycle(schedulerService, jobRunner, config);
    }

    @Test
    public void testLaunchDeletesRunOnceJobOnUnavailable() throws Exception {
        final AbstractSchedulerService schedulerService = mock(AbstractSchedulerService.class);
        final JobLauncher jobLauncher = new JobLauncher(schedulerService, RUN_LOCALLY, NOW, JOB_ID);
        final JobDetails jobDetails =
                new UnusableJobDetails(JOB_ID, KEY, RUN_LOCALLY, Schedule.runOnce(null), null, null, null);

        when(schedulerService.getJobDetails(JOB_ID)).thenReturn(jobDetails);

        jobLauncher.launch();

        verify(schedulerService).addRunDetails(JOB_ID, NOW, UNAVAILABLE, "Job runner key 'test.key' is not registered");
        verify(schedulerService).unscheduleJob(JOB_ID);
    }

    @Test
    public void testLaunchJobRunnerDoesNotAttemptToDeleteWithoutJobDetails() throws Exception {
        final AbstractSchedulerService schedulerService = mock(AbstractSchedulerService.class);
        final JobLauncher jobLauncher = new JobLauncher(schedulerService, RUN_LOCALLY, NOW, JOB_ID);

        jobLauncher.launch();

        verify(schedulerService).addRunDetails(JOB_ID, NOW, ABORTED, "No corresponding job details");
        verify(schedulerService, never()).unscheduleJob(JOB_ID);
    }

    private static void assertCloseToNow(final Date date) {
        assertNotNull("Expected a date close to now but got null", date);
        final long delta = abs(currentTimeMillis() - date.getTime());
        assertThat("Expected date to be close to now but the delta was too large", delta, lessThan(1000L));
    }

    private static void assertJobLifeCycle(
            AbstractSchedulerService schedulerService, JobRunner jobRunner, JobConfig config) {
        final InOrder inOrder = inOrder(schedulerService, jobRunner);
        final ArgumentCaptor<RunningJob> jobCaptor = ArgumentCaptor.forClass(RunningJob.class);
        inOrder.verify(schedulerService).enterJob(eq(JOB_ID), jobCaptor.capture());
        inOrder.verify(schedulerService).preJob();
        inOrder.verify(jobRunner).runJob(new RunningJobImpl(NOW, JOB_ID, config));
        inOrder.verify(schedulerService).leaveJob(JOB_ID, jobCaptor.getValue());
        inOrder.verify(schedulerService).postJob();
    }

    private static JobConfig config() {
        return config(details());
    }

    private static JobConfig config(JobDetails jobDetails) {
        return JobConfig.forJobRunnerKey(jobDetails.getJobRunnerKey())
                .withRunMode(jobDetails.getRunMode())
                .withSchedule(jobDetails.getSchedule())
                .withParameters(jobDetails.getParameters());
    }

    private static SimpleJobDetails details() {
        return new SimpleJobDetails(JOB_ID, KEY, RUN_LOCALLY, SCHEDULE, null, null, null);
    }

    private static UnusableJobDetails unusable(String reason) {
        return new UnusableJobDetails(
                JOB_ID,
                KEY,
                RUN_LOCALLY,
                SCHEDULE,
                null,
                null,
                (reason != null) ? new IllegalStateException("Bet you didn't see this coming!") : null);
    }
}
