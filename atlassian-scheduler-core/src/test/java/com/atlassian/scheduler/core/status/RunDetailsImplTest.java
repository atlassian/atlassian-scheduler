package com.atlassian.scheduler.core.status;

import java.util.Date;

import org.junit.Test;

import static java.util.Objects.requireNonNull;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.startsWith;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import static com.atlassian.scheduler.core.status.RunDetailsImpl.MAXIMUM_MESSAGE_LENGTH;
import static com.atlassian.scheduler.status.RunOutcome.ABORTED;
import static com.atlassian.scheduler.status.RunOutcome.FAILED;
import static com.atlassian.scheduler.status.RunOutcome.SUCCESS;

/**
 * @since v1.0
 */
@SuppressWarnings({"ResultOfObjectAllocationIgnored", "ConstantConditions"})
public class RunDetailsImplTest {
    private static final Date NOW = new Date();

    @Test(expected = NullPointerException.class)
    public void testLastRunTimeNull() {
        new RunDetailsImpl(null, ABORTED, 0L, "Info");
    }

    @Test
    public void testLastRunTimeIsDefensivelyCopied() {
        final Date original = new Date();
        final long originalTime = original.getTime();
        final RunDetailsImpl js = new RunDetailsImpl(original, ABORTED, 0L, "Info");
        original.setTime(42L);

        Date copy = requireNonNull(js.getStartTime(), "copy");
        assertThat(
                "Modifying the original after it is submitted does not change the value",
                copy.getTime(),
                is(originalTime));

        copy.setTime(42L);
        copy = requireNonNull(js.getStartTime(), "copy");
        assertThat("Modifying the returned copy does not change the value", copy.getTime(), is(originalTime));
    }

    @Test(expected = NullPointerException.class)
    public void testRunOutcomeNull() {
        new RunDetailsImpl(NOW, null, 0L, "Info");
    }

    @Test
    public void testDurationIsNegative() {
        assertEquals(
                "Negative durations should force to 0L",
                0L,
                new RunDetailsImpl(NOW, ABORTED, -42L, "Info").getDurationInMillis());
    }

    @Test
    public void testInfoIsNull() {
        assertThat(new RunDetailsImpl(NOW, ABORTED, 0L, null).getMessage(), is(""));
    }

    @Test
    public void testMessageIsMaximumLength() {
        final String message = generate(MAXIMUM_MESSAGE_LENGTH);
        final RunDetailsImpl js = new RunDetailsImpl(NOW, ABORTED, 0L, message);
        assertThat(js.getMessage(), is(message));
    }

    @Test
    public void testMessageExceedsMaximumLength() {
        final String message = generate(MAXIMUM_MESSAGE_LENGTH + 42);
        final RunDetailsImpl js = new RunDetailsImpl(NOW, ABORTED, 0L, message);
        assertNotNull(js.getMessage());
        assertThat(message, startsWith(js.getMessage()));
        assertThat(js.getMessage().length(), is(MAXIMUM_MESSAGE_LENGTH));
    }

    @Test
    public void testValues1() {
        final Date later = new Date(NOW.getTime() + 42L);
        final RunDetailsImpl js = new RunDetailsImpl(later, SUCCESS, 42L, "Info");
        assertThat(js.getStartTime(), is(later));
        assertThat(js.getRunOutcome(), is(SUCCESS));
        assertThat(js.getDurationInMillis(), is(42L));
        assertThat(js.getMessage(), is("Info"));
    }

    @Test
    public void testValues2() {
        final Date later = new Date(NOW.getTime() + 42L);
        final RunDetailsImpl js = new RunDetailsImpl(later, FAILED, 2495L, "Hello, world!");
        assertThat(js.getStartTime(), is(later));
        assertThat(js.getRunOutcome(), is(FAILED));
        assertThat(js.getDurationInMillis(), is(2495L));
        assertThat(js.getMessage(), is("Hello, world!"));
    }

    /**
     * Generates a string of the requested length and comprised of printable ASCII characters (from '!' to '~')
     *
     * @param len the desired length of the string
     * @return the generated string
     */
    private static String generate(final int len) {
        char c = '!';
        final StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; ++i) {
            sb.append(c);
            if (c >= '~') {
                c = '!';
            } else {
                c++; // :P
            }
        }
        return sb.toString();
    }
}
