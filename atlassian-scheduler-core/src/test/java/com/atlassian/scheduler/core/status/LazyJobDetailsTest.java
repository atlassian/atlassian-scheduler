package com.atlassian.scheduler.core.status;

import java.util.Date;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.core.AbstractSchedulerService;
import com.atlassian.scheduler.core.util.ParameterMapSerializer;
import com.atlassian.scheduler.status.JobDetails;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import static com.atlassian.scheduler.config.RunMode.RUN_LOCALLY;
import static com.atlassian.scheduler.config.RunMode.RUN_ONCE_PER_CLUSTER;
import static com.atlassian.scheduler.config.Schedule.runOnce;
import static com.atlassian.scheduler.core.Constants.BYTES_DEADF00D;
import static com.atlassian.scheduler.core.Constants.BYTES_PARAMETERS;
import static com.atlassian.scheduler.core.Constants.EMPTY_MAP;
import static com.atlassian.scheduler.core.Constants.JOB_ID;
import static com.atlassian.scheduler.core.Constants.KEY;
import static com.atlassian.scheduler.core.Constants.PARAMETERS;

/**
 * @since v1.0
 */
@SuppressWarnings({"ConstantConditions", "ResultOfObjectAllocationIgnored"})
public class LazyJobDetailsTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private AbstractSchedulerService schedulerService;

    @Before
    public void setUp() {
        when(schedulerService.getParameterMapSerializer()).thenReturn(new ParameterMapSerializer());
    }

    @Test(expected = NullPointerException.class)
    public void testSchedulerServiceNull() {
        new LazyJobDetails(null, JOB_ID, KEY, RUN_LOCALLY, runOnce(null), new Date(), BYTES_PARAMETERS);
    }

    @Test(expected = NullPointerException.class)
    public void testJobIdNull() {
        new LazyJobDetails(schedulerService, null, KEY, RUN_LOCALLY, runOnce(null), new Date(), BYTES_PARAMETERS);
    }

    @Test(expected = NullPointerException.class)
    public void testJobRunnerKeyNull() {
        new LazyJobDetails(schedulerService, JOB_ID, null, RUN_LOCALLY, runOnce(null), new Date(), BYTES_PARAMETERS);
    }

    @Test(expected = NullPointerException.class)
    public void testRunModeNull() {
        new LazyJobDetails(schedulerService, JOB_ID, KEY, null, runOnce(null), new Date(), BYTES_PARAMETERS);
    }

    @Test(expected = NullPointerException.class)
    public void testScheduleNull() {
        new LazyJobDetails(schedulerService, JOB_ID, KEY, RUN_LOCALLY, null, new Date(), BYTES_PARAMETERS);
    }

    @Test
    public void testValues1() {
        final Date nextRunTime = new Date();
        final Date expectedNextRunTime = new Date(nextRunTime.getTime());

        when(schedulerService.getJobRunner(KEY)).thenReturn(mock(JobRunner.class));

        final JobDetails jobDetails =
                new LazyJobDetails(schedulerService, JOB_ID, KEY, RUN_LOCALLY, runOnce(null), nextRunTime, null);

        assertEquals(JOB_ID, jobDetails.getJobId());
        assertEquals(KEY, jobDetails.getJobRunnerKey());
        assertEquals(RUN_LOCALLY, jobDetails.getRunMode());
        assertEquals(runOnce(null), jobDetails.getSchedule());

        assertTrue("Should be runnable", jobDetails.isRunnable());
        assertEquals(EMPTY_MAP, jobDetails.getParameters());

        nextRunTime.setTime(42L);
        assertEquals(expectedNextRunTime, jobDetails.getNextRunTime());
        jobDetails.getNextRunTime().setTime(42L);
        assertEquals(expectedNextRunTime, jobDetails.getNextRunTime());
    }

    @Test
    public void testValues2() {
        when(schedulerService.getJobRunner(JobRunnerKey.of("z"))).thenReturn(mock(JobRunner.class));

        final JobDetails jobDetails = new LazyJobDetails(
                schedulerService,
                JobId.of("x"),
                JobRunnerKey.of("z"),
                RUN_ONCE_PER_CLUSTER,
                runOnce(new Date(42L)),
                null,
                BYTES_DEADF00D);

        assertEquals(JobId.of("x"), jobDetails.getJobId());
        assertEquals(JobRunnerKey.of("z"), jobDetails.getJobRunnerKey());
        assertEquals(RUN_ONCE_PER_CLUSTER, jobDetails.getRunMode());
        assertEquals(runOnce(new Date(42L)), jobDetails.getSchedule());
        assertNull(jobDetails.getNextRunTime());

        assertFalse("Should not be runnable", jobDetails.isRunnable());
    }

    @Test
    public void testDeserializeParameters() {
        final JobRunner jobRunner = mock(JobRunner.class);
        when(jobRunner.getParametersClassLoader()).thenReturn(getClass().getClassLoader());
        when(schedulerService.getJobRunner(KEY)).thenReturn(jobRunner);

        final JobDetails jobDetails = new LazyJobDetails(
                schedulerService, JOB_ID, KEY, RUN_LOCALLY, runOnce(null), new Date(), BYTES_PARAMETERS);

        assertEquals(PARAMETERS, jobDetails.getParameters());
    }
}
