/**
 * Core components that will likely be needed by any implementation of the atlassian-scheduler API.
 */
@ParametersAreNonnullByDefault
package com.atlassian.scheduler.core;

import javax.annotation.ParametersAreNonnullByDefault;
