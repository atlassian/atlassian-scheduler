package com.atlassian.scheduler.core.status;

import java.util.Date;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.annotation.concurrent.Immutable;

import com.atlassian.scheduler.status.RunDetails;
import com.atlassian.scheduler.status.RunOutcome;

import static java.util.Objects.requireNonNull;

@Immutable
public final class RunDetailsImpl implements RunDetails {
    private final long startTime;
    private final RunOutcome runOutcome;
    private final long durationInMillis;
    private final String message;

    public RunDetailsImpl(
            final Date startTime,
            final RunOutcome runOutcome,
            final long durationInMillis,
            @Nullable final String message) {
        this.startTime = requireNonNull(startTime, "startTime").getTime();
        this.runOutcome = requireNonNull(runOutcome, "runOutcome");
        this.durationInMillis = (durationInMillis >= 0L) ? durationInMillis : 0L;
        this.message = truncate(message);
    }

    @Override
    @Nonnull
    public Date getStartTime() {
        return new Date(startTime);
    }

    @Override
    public long getDurationInMillis() {
        return durationInMillis;
    }

    @Override
    @Nonnull
    public RunOutcome getRunOutcome() {
        return runOutcome;
    }

    @Override
    @Nonnull
    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return "RunDetailsImpl[startTime=" + startTime + ",runOutcome="
                + runOutcome + ",durationInMillis="
                + durationInMillis + ",message="
                + message + ']';
    }

    private static String truncate(@Nullable final String message) {
        if (message == null) {
            return "";
        }
        if (message.length() > MAXIMUM_MESSAGE_LENGTH) {
            return message.substring(0, MAXIMUM_MESSAGE_LENGTH);
        }
        return message;
    }
}
