package com.atlassian.scheduler.core;

import java.util.List;
import java.util.Map;
import javax.annotation.CheckForNull;

import com.atlassian.scheduler.SchedulerHistoryService;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.core.spi.RunDetailsDao;
import com.atlassian.scheduler.status.RunDetails;

/**
 * @since 1.0
 */
public class DefaultSchedulerHistoryService implements SchedulerHistoryService {
    private final RunDetailsDao runDetailsDao;

    public DefaultSchedulerHistoryService(RunDetailsDao runDetailsDao) {
        this.runDetailsDao = runDetailsDao;
    }

    @Override
    @CheckForNull
    public RunDetails getLastSuccessfulRunForJob(JobId jobId) {
        return runDetailsDao.getLastSuccessfulRunForJob(jobId);
    }

    @CheckForNull
    @Override
    public Map<JobId, RunDetails> getLastRunForJobs(List<JobId> jobIds) {
        return runDetailsDao.getLastRunForJobs(jobIds);
    }

    @Override
    @CheckForNull
    public RunDetails getLastRunForJob(JobId jobId) {
        return runDetailsDao.getLastRunForJob(jobId);
    }
}
