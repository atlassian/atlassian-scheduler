package com.atlassian.scheduler.core.util;

import org.slf4j.Logger;

/**
 * Log a warning, optionally including a full stack trace if the logger's {@code DEBUG} level is enabled.
 */
public class LogWarn {
    // Static-only
    private LogWarn() {}

    /**
     * Log a warning, optionally including a full stack trace if the logger's {@code DEBUG} level is enabled.
     * <p>
     * That is, if {@code DEBUG} is enabled, the logger gets the warning with a full stack trace included.
     * Otherwise, the exception's {@code .toString()} is appended to the message, keeping it comparatively brief.
     * </p>
     *
     * @param log     the logger to receive the warning message
     * @param message the message to be logged
     * @param cause   the exception that triggered the warning
     */
    public static void logWarn(Logger log, String message, Throwable cause) {
        if (log.isDebugEnabled()) {
            log.warn(message, cause);
        } else {
            log.warn(message + ": " + cause);
        }
    }
}
