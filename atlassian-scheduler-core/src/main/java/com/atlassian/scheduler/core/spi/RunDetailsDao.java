package com.atlassian.scheduler.core.spi;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.CheckForNull;

import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.status.RunDetails;

/**
 * Service provided by the host application to persist {@code RunDetails} objects.
 *
 * @since v1.0
 */
public interface RunDetailsDao {
    /**
     * Returns the result of the most recent attempt to run this job.
     *
     * @param jobId the job ID of interest
     * @return the result information for the most recent run attempt, or {@code null} if there
     * is no recorded run history for this job
     */
    @CheckForNull
    RunDetails getLastRunForJob(JobId jobId);

    /**
     * Returns the result of the most recent successful run of this job.
     *
     * @param jobId the job ID of interest
     * @return the result information for the most recent run attempt, or {@code null} if there
     * is no successful result recorded for this job
     */
    @CheckForNull
    RunDetails getLastSuccessfulRunForJob(JobId jobId);

    /**
     * Return results of the most recent attempt to run these jobs, grouped by {@code JobId}
     * of that job.
     * Implementation of this method is intended to use query with IN statement.
     * Oracle has a limit of 1000 elements on IN statement,
     * hence if there are more than 1000 elements in passed list, the query should be split into batches.
     * @param jobIds list of job IDs of interest
     * @return the result information for the most recent runs attempt grouped by job's id, or empty {@code Map} if there
     * is no recorded run history for these jobs.
     * Default implementation iterates through the provided list, calls {@link #getLastRunForJob(JobId)} for each element,
     * and gathers results in a map whereas {@link JobId} is a key and {@link RunDetails} is a value.
     */
    default Map<JobId, RunDetails> getLastRunForJobs(List<JobId> jobIds) {
        Map<JobId, RunDetails> runDetailsByJobId = new HashMap<>();

        for (JobId jobId : jobIds) {
            RunDetails runDetails = getLastRunForJob(jobId);
            if (runDetails != null) {
                runDetailsByJobId.put(jobId, runDetails);
            }
        }

        return runDetailsByJobId;
    }

    /**
     * Records the result of an attempt to run the specified job.
     *
     * @param jobId      the job ID of the job that the scheduler attempted to run
     * @param runDetails the result of the run
     */
    void addRunDetails(JobId jobId, RunDetails runDetails);
}
