package com.atlassian.scheduler.core;

import com.atlassian.scheduler.JobRunnerRequest;

/**
 * Represents a particular instance of a running job.
 * <p>
 * This adds management capabilities to a job runner request.
 * </p>
 *
 * @since v1.3
 */
public interface RunningJob extends JobRunnerRequest {
    /**
     * Requests that the job be cancelled.
     * <p>
     * Calling this method changes the return value of {@link #isCancellationRequested()} permanently
     * to {@code true}.  It has no other effect; in particular, it does not interrupt the thread that
     * is running the job.  It is the responsibility of the job runner implementation to cooperatively
     * check for an respond to the {@link #isCancellationRequested()} flag.
     * </p>
     */
    void cancel();
}
