package com.atlassian.scheduler.core;

import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobRunnerKey;

/**
 * @since v1.0
 */
public class JobRunnerNotRegisteredException extends SchedulerServiceException {
    private static final long serialVersionUID = 1L;

    private final JobRunnerKey jobRunnerKey;

    public JobRunnerNotRegisteredException(final JobRunnerKey jobRunnerKey) {
        super("No job runner registered for job runner key '" + jobRunnerKey + '\'');
        this.jobRunnerKey = jobRunnerKey;
    }

    public JobRunnerKey getJobRunnerKey() {
        return jobRunnerKey;
    }
}
