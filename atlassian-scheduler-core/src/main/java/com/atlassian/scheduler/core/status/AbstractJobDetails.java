package com.atlassian.scheduler.core.status;

import java.util.Date;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;
import com.atlassian.scheduler.config.RunMode;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.status.JobDetails;

import static java.util.Objects.requireNonNull;

import static com.atlassian.scheduler.util.Safe.copy;

/**
 * Base implementation for {@link JobDetails}.
 *
 * @since v1.0
 */
public abstract class AbstractJobDetails implements JobDetails {
    protected final JobId jobId;
    protected final JobRunnerKey jobRunnerKey;
    protected final RunMode runMode;
    protected final Schedule schedule;

    private final Date nextRunTime;
    private final byte[] rawParameters;

    protected AbstractJobDetails(
            JobId jobId,
            JobRunnerKey jobRunnerKey,
            RunMode runMode,
            Schedule schedule,
            @Nullable Date nextRunTime,
            @Nullable byte[] rawParameters) {
        this.jobId = requireNonNull(jobId, "jobId");
        this.jobRunnerKey = requireNonNull(jobRunnerKey, "jobRunnerKey");
        this.runMode = requireNonNull(runMode, "runMode");
        this.schedule = requireNonNull(schedule, "schedule");
        this.nextRunTime = copy(nextRunTime);
        this.rawParameters = copy(rawParameters);
    }

    @Nonnull
    public final JobId getJobId() {
        return jobId;
    }

    @Nonnull
    public final JobRunnerKey getJobRunnerKey() {
        return jobRunnerKey;
    }

    @Nonnull
    public final RunMode getRunMode() {
        return runMode;
    }

    @Nonnull
    public Schedule getSchedule() {
        return schedule;
    }

    @Nullable
    public Date getNextRunTime() {
        return copy(nextRunTime);
    }

    /**
     * Returns the raw bytes from the job's parameters.
     * <p>
     * This is not part of the public API.  It is intended for the persistence layer to use for accessing the raw
     * parameters and/or a troubleshooting tool for a management interface to use.  For example, it might try to
     * deserialize the map with the application's class loader or read the raw serialization data to show the
     * administrator whatever information can be pulled out of it.
     * </p>
     *
     * @return a copy of the raw bytes that encode the job's parameters, or {@code null} if the parameter map
     * was empty.
     */
    @Nullable
    public final byte[] getRawParameters() {
        return copy(rawParameters);
    }

    @Override
    public final String toString() {
        final StringBuilder sb = new StringBuilder(128)
                .append(getClass().getSimpleName())
                .append("[jobId=")
                .append(jobId)
                .append(",jobRunnerKey=")
                .append(jobRunnerKey)
                .append(",runMode=")
                .append(runMode)
                .append(",schedule=")
                .append(schedule)
                .append(",nextRunTime=")
                .append(nextRunTime)
                .append(",rawParameters=(");
        if (rawParameters == null) {
            sb.append("null)");
        } else {
            sb.append(rawParameters.length).append(" bytes)");
        }
        appendToStringDetails(sb);
        return sb.append(']').toString();
    }

    protected abstract void appendToStringDetails(final StringBuilder sb);
}
