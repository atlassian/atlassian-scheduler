package com.atlassian.scheduler.caesium.impl;

import java.io.Serializable;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.scheduler.config.JobId;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.requireNonNull;

/**
 * Represents a request to run a job at a particular time.
 *
 * @since v0.0.1
 */
@SuppressWarnings("SerializableHasSerializationMethods") // the default read/write are fine
public final class QueuedJob implements Serializable, Comparable<QueuedJob> {
    private static final long serialVersionUID = -3588231346686548223L;

    private final JobId jobId;
    private final long deadline;

    /**
     * @param jobId    the {@link JobId} for the job that will run at the given time
     * @param deadline when (in terms of {@link System#currentTimeMillis()}), the job should run; must not be
     *                 negative, but values in the past, such as {@code 0L}, are otherwise permitted.
     */
    QueuedJob(@Nonnull final JobId jobId, final long deadline) {
        this.jobId = requireNonNull(jobId, "jobId");
        this.deadline = deadline;
        checkArgument(deadline >= 0L, "deadline cannot be negative");
    }

    public JobId getJobId() {
        return jobId;
    }

    public long getDeadline() {
        return deadline;
    }

    @Override
    public boolean equals(@Nullable Object o) {
        return this == o || (o instanceof QueuedJob && equals((QueuedJob) o));
    }

    private boolean equals(QueuedJob other) {
        return deadline == other.deadline && jobId.equals(other.jobId);
    }

    @Override
    public int hashCode() {
        return 31 * jobId.hashCode() + (int) (deadline ^ (deadline >>> 32));
    }

    @Override
    public int compareTo(final QueuedJob other) {
        if (deadline < other.deadline) {
            return -1;
        }
        if (deadline > other.deadline) {
            return 1;
        }
        return jobId.compareTo(other.jobId);
    }

    @Override
    public String toString() {
        return "QueuedJob[jobId=" + jobId + ",deadline=" + deadline + ']';
    }
}
