package com.atlassian.scheduler.caesium.cron.rule;

import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.zone.ZoneOffsetTransition;
import java.util.Date;
import javax.annotation.Nullable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A holder for the various numbers that comprise a date and time for the purpose of cron calculations.
 * We do not want to be forbidden to set a value at all when it breaks the rules.  This is what
 * {@code DateTime} does, so we can't use that.  Neither do we want the implementation to be so forgiving
 * that when we ask for February 30th it decides that we really mean March 1st or 2nd, and that means
 * that {@code LocalDateTime} is also a problem.  What we really want is to be able to say "this will
 * match on February 30th if that exists; if it doesn't, then I want to move on to the next thing to try,
 * which is probably either March 1st or March 30th depending on what the other rules are, but let me
 * worry about exactly how that works."
 * </p><p>
 * Well, fine.  Think about this as the {@code MutablePartial} that should have been but never was, and
 * with a better name to boot.
 * </p>
 *
 * @since v0.0.1
 */
public class DateTimeTemplate implements Cloneable {
    private static final Logger LOG = LoggerFactory.getLogger(DateTimeTemplate.class);

    /**
     * Lets us pre-flight check a month and day so we can preclude pathological/abusive cases where somebody
     * can easily trigger a flurry of exceptions by asking for the 31st of the month from every month that
     * doesn't actually have one.
     */
    private static final int[] MAX_DAY_BY_MONTH = {-1, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    private final ZoneId zone;
    private int year;
    private int month;
    private int day;
    private int hour;
    private int minute;
    private int second;

    public DateTimeTemplate(final ZonedDateTime dateTime) {
        this.zone = ZoneId.of(dateTime.getZone().getId());
        this.year = dateTime.getYear();
        this.month = dateTime.getMonthValue();
        this.day = dateTime.getDayOfMonth();
        this.hour = dateTime.getHour();
        this.minute = dateTime.getMinute();
        this.second = dateTime.getSecond();
    }

    public DateTimeTemplate(final Date date, final ZoneId zone) {
        this(date.toInstant().atZone(zone));
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public ZoneId getZoneId() {
        return zone;
    }

    /**
     * Returns the first day of the month for the current year and month values, which must be valid.
     *
     * @return the first day of the month for the current year and month values
     * @throws IllegalArgumentException if the year or month is invalid
     */
    public LocalDate toFirstDayOfMonth() {
        return LocalDate.of(year, month, 1);
    }

    /**
     * Converts this template into an immutable {@code DateTime} representation, if possible.
     * <p>
     * As this object holds a set of numbers describing a hypothetical date and time that may or may not
     * actually exist (due to differing days per month and existence of specific times at daylight savings
     * transition gaps), it may not be possible to satisfy this request.  If the combination of values is
     * invalid, then {@code null} is returned.
     * </p><p>
     * For consistency with Quartz's arbitrary choice, if the numbers given fall within a daylight savings
     * overlap, then the <strong>later</strong> instant is selected.  For example, in the Australia/Sydney
     * time zone, the clock moved from 2 A.M. back to 1 A.M. on 2014/04/06.  The date-time values for
     * {@code 2014-04-06 01:30:00} return the moment corresponding to the 1:30 A.M. that occurs
     * <strong>after</strong> the daylight savings transition, not the one before it.
     * </p>
     */
    @Nullable
    public ZonedDateTime toZonedDateTime() {
        if (!isPlausible()) {
            return null;
        }
        try {
            LocalDateTime localDateTime = LocalDateTime.of(year, month, day, hour, minute, second);
            ZoneOffsetTransition transition = zone.getRules().getTransition(localDateTime);
            if (transition == null || !transition.isGap()) {
                return ZonedDateTime.of(localDateTime, zone).withLaterOffsetAtOverlap();
            } else {
                return null;
            }

        } catch (DateTimeException dte) {
            LOG.debug("Invalid date: {}", this, dte);
            return null;
        }
    }

    /**
     * Returns {@code false} if the current values are obviously not valid because the day-of-month is absurd
     * for the month given.
     *
     * @return {@code true} if the year, month, and day-of-month are a plausible combination; {@code false} if
     * it is obvious that they are not valid
     */
    private boolean isPlausible() {
        // Weed out obvious garbage like February 30th or April 31st.
        if (month < 1 || month > 12 || day > MAX_DAY_BY_MONTH[month]) {
            return false;
        }

        // Also weed out February 29th if the year isn't a multiple for 4.
        return day != 29 || month != 2 || (year & 3) == 0;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder(64).append(year);
        append2(sb, '/', month);
        append2(sb, '/', day);
        append2(sb, ' ', hour);
        append2(sb, ':', minute);
        append2(sb, ':', second);
        return sb.append(" (").append(zone).append(')').toString();
    }

    private static void append2(final StringBuilder sb, final char delimiter, final int value) {
        sb.append(delimiter);
        if (value < 10) {
            sb.append('0');
        }
        sb.append(value);
    }

    /**
     * Represent one of the base fields of date information.
     */
    public enum Field {
        YEAR(1970, 2999) {
            @Override
            public int get(DateTimeTemplate dateTime) {
                return dateTime.getYear();
            }

            @Override
            public void set(DateTimeTemplate dateTime, int value) {
                dateTime.setYear(value);
            }
        },

        MONTH(1, 12) {
            @Override
            public int get(DateTimeTemplate dateTime) {
                return dateTime.getMonth();
            }

            @Override
            public void set(DateTimeTemplate dateTime, int value) {
                dateTime.setMonth(value);
            }
        },

        DAY(1, 31) {
            @Override
            public int get(DateTimeTemplate dateTime) {
                return dateTime.getDay();
            }

            @Override
            public void set(DateTimeTemplate dateTime, int value) {
                dateTime.setDay(value);
            }
        },

        HOUR(0, 23) {
            @Override
            public int get(DateTimeTemplate dateTime) {
                return dateTime.getHour();
            }

            @Override
            public void set(DateTimeTemplate dateTime, int value) {
                dateTime.setHour(value);
            }
        },

        MINUTE(0, 59) {
            @Override
            public int get(DateTimeTemplate dateTime) {
                return dateTime.getMinute();
            }

            @Override
            public void set(DateTimeTemplate dateTime, int value) {
                dateTime.setMinute(value);
            }
        },

        SECOND(0, 59) {
            @Override
            public int get(DateTimeTemplate dateTime) {
                return dateTime.getSecond();
            }

            @Override
            public void set(DateTimeTemplate dateTime, int value) {
                dateTime.setSecond(value);
            }
        };

        private final int minimumValue;
        private final int maximumValue;

        private Field(int minimumValue, int maximumValue) {
            this.minimumValue = minimumValue;
            this.maximumValue = maximumValue;
        }

        public abstract int get(DateTimeTemplate dateTime);

        public abstract void set(DateTimeTemplate dateTime, int value);

        public int getMinimumValue() {
            return minimumValue;
        }

        public int getMaximumValue() {
            return maximumValue;
        }
    }
}
