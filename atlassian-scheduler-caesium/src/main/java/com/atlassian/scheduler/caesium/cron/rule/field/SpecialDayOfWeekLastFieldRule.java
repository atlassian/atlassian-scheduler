package com.atlassian.scheduler.caesium.cron.rule.field;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;

import static com.google.common.base.Preconditions.checkArgument;

import static com.atlassian.scheduler.caesium.cron.rule.field.DayOfWeekConstantConverter.cronToIso;
import static com.atlassian.scheduler.caesium.cron.rule.field.DayOfWeekConstantConverter.isoToCron;

/**
 * Special rule for the last occurrence of the given day-of-week, such as the last Friday every month.
 *
 * @since v0.0.1
 */
public class SpecialDayOfWeekLastFieldRule extends SpecialDayFieldRule {
    private static final long serialVersionUID = 4480059862309727633L;

    private final int isoDayOfWeek; // 1=Monday

    public SpecialDayOfWeekLastFieldRule(int cronDayOfWeek) {
        super();
        checkArgument(cronDayOfWeek >= 1 && cronDayOfWeek <= 7, "cronDayOfWeek must be in the range [1,7]");

        this.isoDayOfWeek = cronToIso(cronDayOfWeek);
    }

    @Override
    int calculateMatchingDay(final int year, final int month) {
        final LocalDate firstDay = LocalDate.of(year, month, 1);
        final LocalDate lastDayOfMonth = firstDay.withDayOfMonth(firstDay.lengthOfMonth());
        DayOfWeek targetDayOfWeek = DayOfWeek.of(isoDayOfWeek);
        LocalDate date;
        if (lastDayOfMonth.getDayOfWeek().getValue() < isoDayOfWeek) {
            date = lastDayOfMonth.with(TemporalAdjusters.nextOrSame(targetDayOfWeek));
        } else {
            date = lastDayOfMonth.with(TemporalAdjusters.previousOrSame(targetDayOfWeek));
        }
        if (date.isAfter(lastDayOfMonth)) {
            date = date.minusWeeks(1);
        }
        return date.getDayOfMonth();
    }

    @Override
    protected void appendTo(StringBuilder sb) {
        sb.append(isoToCron(isoDayOfWeek)).append('L');
    }
}
