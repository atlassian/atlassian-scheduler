package com.atlassian.scheduler.caesium.cron.rule.field;

import com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate;
import com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate.Field;

/**
 * Rules based on day-of-month or day-of-week that uses special flags like {@code L}, {@code W}, or {@code #}
 * and is therefore guaranteed to match at most one day in any given month.
 *
 * @since v0.0.1
 */
abstract class SpecialDayFieldRule extends AbstractFieldRule {
    private static final long serialVersionUID = -5909551168217552683L;

    SpecialDayFieldRule() {
        super(Field.DAY);
    }

    @Override
    public boolean matches(DateTimeTemplate dateTime) {
        return get(dateTime) == getMatchingDay(dateTime);
    }

    @Override
    public boolean first(DateTimeTemplate dateTime) {
        final int day = getMatchingDay(dateTime);
        if (day == -1) {
            return false;
        }
        set(dateTime, day);
        return true;
    }

    @Override
    public boolean next(DateTimeTemplate dateTime) {
        final int day = getMatchingDay(dateTime);
        if (day == -1 || get(dateTime) >= day) {
            return false;
        }
        set(dateTime, day);
        return true;
    }

    /**
     * Returns the only day this month that will match, or {@code -1} if a match is impossible for the current month.
     *
     * @return the only day this month that will match, or {@code -1} if a match is impossible for the current month.
     */
    private int getMatchingDay(final DateTimeTemplate dateTime) {
        final int year = dateTime.getYear();
        final int month = dateTime.getMonth();
        return calculateMatchingDay(year, month);
    }

    /**
     * Based on the specified year and month, calculates the only day of month value that will match the rule.
     * If there is no value that will match the rule, then {@code -1} is returned, instead.
     *
     * @param year  the year to consider, which is expected to be in the range (1970, 2299)
     * @param month the month to consider, as an integer in the range (1, 12)
     * @return the day of the month that matches this rule, or {@code -1} if it cannot be matched by any day this month
     */
    abstract int calculateMatchingDay(final int year, final int month);
}
