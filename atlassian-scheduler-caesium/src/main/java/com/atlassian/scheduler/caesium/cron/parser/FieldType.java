package com.atlassian.scheduler.caesium.cron.parser;

import com.atlassian.scheduler.caesium.cron.parser.CronLexer.Token;
import com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate;
import com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate.Field;
import com.atlassian.scheduler.caesium.cron.rule.field.FieldRule;
import com.atlassian.scheduler.caesium.cron.rule.field.RangeFieldRule;
import com.atlassian.scheduler.cron.CronSyntaxException;
import com.atlassian.scheduler.cron.ErrorCode;

import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NAME;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NAME_FIELD;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NUMBER_DAY_OF_MONTH;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NUMBER_DAY_OF_WEEK;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NUMBER_HOUR;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NUMBER_MONTH;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NUMBER_SEC_OR_MIN;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NUMBER_YEAR;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_STEP;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_STEP_DAY_OF_MONTH;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_STEP_DAY_OF_WEEK;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_STEP_HOUR;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_STEP_MONTH;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_STEP_SECOND_OR_MINUTE;

/**
 * The various cron fields and their parsing behaviours.
 *
 * @since v0.0.1
 */
enum FieldType {
    SECOND(INVALID_NUMBER_SEC_OR_MIN, INVALID_STEP_SECOND_OR_MINUTE, Field.SECOND),
    MINUTE(INVALID_NUMBER_SEC_OR_MIN, INVALID_STEP_SECOND_OR_MINUTE, Field.MINUTE),
    HOUR(INVALID_NUMBER_HOUR, INVALID_STEP_HOUR, Field.HOUR),
    DAY_OF_MONTH(INVALID_NUMBER_DAY_OF_MONTH, INVALID_STEP_DAY_OF_MONTH, Field.DAY),
    MONTH(INVALID_NUMBER_MONTH, INVALID_STEP_MONTH, Field.MONTH) {
        @Override
        int resolveName(Token token) throws CronSyntaxException {
            return NameResolver.MONTH.resolveName(token);
        }
    },
    DAY_OF_WEEK(INVALID_NUMBER_DAY_OF_WEEK, INVALID_STEP_DAY_OF_WEEK, Field.DAY) {
        @Override
        int resolveName(Token token) throws CronSyntaxException {
            return NameResolver.DAY_OF_WEEK.resolveName(token);
        }

        /**
         * Returns {@code 7}, the maximum valid day-of-week value.
         * The other fields just inherit their maximum values from the {@link DateTimeTemplate.Field} that they
         * target; however the day-of-week calculations manipulate the day-of-month, so it would otherwise give
         * the wrong answer, here.
         *
         * @return {@code 7}
         */
        @Override
        int getMaximumValue() {
            return 7;
        }
    },
    YEAR(INVALID_NUMBER_YEAR, INVALID_STEP, Field.YEAR);

    private final ErrorCode valueErrorCode;
    private final ErrorCode stepErrorCode;
    private final Field field;
    private final FieldRule all;

    private FieldType(ErrorCode valueErrorCode, ErrorCode stepErrorCode, Field field) {
        this.valueErrorCode = valueErrorCode;
        this.stepErrorCode = stepErrorCode;
        this.field = field;
        this.all = RangeFieldRule.of(field, getMinimumValue(), getMaximumValue());
    }

    int getMinimumValue() {
        return field.getMinimumValue();
    }

    int getMaximumValue() {
        return field.getMaximumValue();
    }

    /**
     * Returns the value that needs to be subtracted to "wrap around" an inverted range.
     * <p>
     * For example, consider the month expression {@code OCT-MAY/2}.  First, the names are mapped to values,
     * so logically this becomes {@code 10-5/2}.  Since October comes after May, this wraps around the end
     * of a year.  So as we fill in what values match, we get the sequence: 10 (October), 12 (December),
     * and then 14.  Of course, 14 isn't a valid month number, so what should we wrap around to?  The spirit
     * of {@code /2} is that it means "every other month".  Just as after we accept October we need to skip
     * November, similarly after December we need to skip January.  February is clearly the right answer
     * (followed by 4=April and then stopping because 6=June is past the end value).
     * </p><p>
     * To get this to work, we need to know what it takes to move a value that is past the maximum to the
     * appropriate offset from the minimum if the values are thought of making a circle instead of a line.
     * Mathematically, this is what it takes to map one more than the maximum value onto the minimum value,
     * which is the maximum value minus the minimum value plus {@code 1}.
     * </p><p>
     * That is exactly what this returns.
     * </p>
     *
     * @return {@link #getMaximumValue() max} - {@link #getMinimumValue() min} + 1
     */
    int getWrapOffset() {
        return getMaximumValue() - getMinimumValue() + 1;
    }

    ErrorCode getValueErrorCode() {
        return valueErrorCode;
    }

    ErrorCode getStepErrorCode() {
        return stepErrorCode;
    }

    Field getField() {
        return field;
    }

    int resolveName(Token token) throws CronSyntaxException {
        final String name = token.getText();
        final ErrorCode errorCode = (name.length() >= 3) ? INVALID_NAME_FIELD : INVALID_NAME;
        throw CronSyntaxException.builder()
                .cronExpression(token.getCronExpression())
                .errorCode(errorCode)
                .errorOffset(token.getStart())
                .value(name)
                .build();
    }

    FieldRule all() {
        return all;
    }
}
