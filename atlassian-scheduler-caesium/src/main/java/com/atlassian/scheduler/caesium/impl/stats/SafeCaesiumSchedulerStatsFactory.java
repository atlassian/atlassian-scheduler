package com.atlassian.scheduler.caesium.impl.stats;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.jira.util.stats.JiraStats;

import static com.atlassian.scheduler.caesium.impl.stats.CaesiumSchedulerStats.STATS_NAME;

/**
 * This factory returns {@link ManagedCaesiumSchedulerStats} when atlassian-stats is in classpath and stats are not disabled,
 * and {@link NoOpCaesiumSchedulerStats} otherwise.
 */
public class SafeCaesiumSchedulerStatsFactory {

    private static final Logger LOG = LoggerFactory.getLogger(SafeCaesiumSchedulerStatsFactory.class);

    /**
     * Copied from {@link JiraStats#COMMON_PREFIX}, so that we can use it even without atlassian-stats on the classpath.
     */
    private static final String COMMON_PREFIX = "[JIRA-STATS] ";

    public static CaesiumSchedulerStats create() {
        try {
            if (Boolean.getBoolean("atlassian.SchedulerService.stats.disabled")) {
                LOG.warn("{}[{}] stats disabled", COMMON_PREFIX, STATS_NAME);
                return new NoOpCaesiumSchedulerStats();
            } else {
                return JiraStats.create(
                        ManagedCaesiumSchedulerStats.class, ManagedCaesiumSchedulerStats.Data::new, false);
            }
        } catch (LinkageError e) {
            // Confluence/Bitbucket/Crowd don't have JiraStats
            LOG.info("{}[{}] stats not available in classpath", COMMON_PREFIX, STATS_NAME);
            return new NoOpCaesiumSchedulerStats();
        }
    }
}
