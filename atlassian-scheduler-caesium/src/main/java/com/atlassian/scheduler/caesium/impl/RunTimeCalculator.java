package com.atlassian.scheduler.caesium.impl;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.TimeZone;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.google.common.annotations.VisibleForTesting;

import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.caesium.cron.parser.CronExpressionParser;
import com.atlassian.scheduler.caesium.cron.rule.CronExpression;
import com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate;
import com.atlassian.scheduler.config.CronScheduleInfo;
import com.atlassian.scheduler.config.IntervalScheduleInfo;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.Schedule;
import com.atlassian.scheduler.core.spi.SchedulerServiceConfiguration;
import com.atlassian.scheduler.cron.CronSyntaxException;

import static java.util.Objects.requireNonNull;

/**
 * A utility for calculating the next run time for a given schedule.
 *
 * @since v0.0.1
 */
public class RunTimeCalculator {
    final SchedulerServiceConfiguration config;

    public RunTimeCalculator(final SchedulerServiceConfiguration config) {
        this.config = config;
    }

    /**
     * Calculates the first run time for the given job configuration.
     *
     * @param jobId     the job ID for which to calculate the first run time (used only for error reporting)
     * @param jobConfig the new job's configuration
     * @return the first time that the job will execute
     * @throws SchedulerServiceException if the job will never run
     */
    @Nonnull
    public Date firstRunTime(final JobId jobId, final JobConfig jobConfig) throws SchedulerServiceException {
        final Date nextRunTime = nextRunTime(jobConfig.getSchedule(), null);
        if (nextRunTime == null) {
            throw new SchedulerServiceException("Job '" + jobId + "' would never run: " + jobConfig.getSchedule());
        }
        return nextRunTime;
    }

    /**
     * Calculates the next run time for the given schedule and previous run time.
     *
     * @param schedule    the job's schedule
     * @param prevRunTime the job's previous run time, or {@code null} if it has never run previously
     * @return the job's next run time, or {@code null} if it will never run again
     * @throws CronSyntaxException if the schedule uses a cron expression that is invalid
     */
    @Nullable
    public Date nextRunTime(final Schedule schedule, @Nullable final Date prevRunTime) throws CronSyntaxException {
        requireNonNull(schedule, "schedule");

        switch (schedule.getType()) {
            case INTERVAL:
                return nextRunTime(schedule.getIntervalScheduleInfo(), prevRunTime);
            case CRON_EXPRESSION:
                return nextRunTime(schedule.getCronScheduleInfo(), prevRunTime);
            default:
                throw new IllegalArgumentException("Unsupported schedule type: " + schedule.getType());
        }
    }

    @Nullable
    private Date nextRunTime(final IntervalScheduleInfo info, @Nullable final Date prevRunTime) {
        // First time?
        if (prevRunTime == null) {
            final Date firstRun = info.getFirstRunTime();
            final Date now = now();
            return (firstRun != null && firstRun.getTime() > now.getTime()) ? firstRun : now;
        }

        // Is this a one-shot that has previously run (or is running now)?
        if (info.getIntervalInMillis() == 0L) {
            return null;
        }

        return new Date(prevRunTime.getTime() + info.getIntervalInMillis());
    }

    @Nullable
    private Date nextRunTime(final CronScheduleInfo info, @Nullable Date prevRunTime) throws CronSyntaxException {
        final String cronExpression = info.getCronExpression();
        final TimeZone timeZone = getTimeZone(info);
        final CronExpression cron = CronExpressionParser.parse(cronExpression);
        final Date runTimeForCalc = prevRunTime == null ? now() : prevRunTime;

        // Generate date/times that should satisfy the cron expression until we get one that is valid
        // or exhaust the cron expression's possibilities
        final DateTimeTemplate when = new DateTimeTemplate(runTimeForCalc, timeZone.toZoneId());
        while (cron.next(when)) {
            final ZonedDateTime dateTime = when.toZonedDateTime();
            if (dateTime != null) {
                return new Date(dateTime.toInstant().toEpochMilli());
            }
        }
        return null;
    }

    private TimeZone getTimeZone(final CronScheduleInfo info) {
        TimeZone timeZone = info.getTimeZone();
        if (timeZone == null) {
            timeZone = config.getDefaultTimeZone();
            if (timeZone == null) {
                timeZone = TimeZone.getDefault();
            }
        }
        return timeZone;
    }

    /**
     * Returns {@code new Date()}.
     *
     * @return {@code new Date()}
     */
    @VisibleForTesting
    Date now() {
        return new Date();
    }
}
