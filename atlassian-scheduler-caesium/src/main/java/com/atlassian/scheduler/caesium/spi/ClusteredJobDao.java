package com.atlassian.scheduler.caesium.spi;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.JobRunnerKey;

/**
 * The host application SPI for persisting clustered jobs.
 * <p>
 * Although additional requirements may be specified on individual methods, implementations:
 * </p>
 * <ul>
 * <li><strong>MUST</strong> ensure that {@code JobId} is constrained as {@code UNIQUE}.</li>
 * <li><strong>MUST</strong> preserve the clustered job information across system restarts.</li>
 * <li><strong>SHOULD</strong> query the least amount of information as is possible to satisfy the request</li>
 * </ul>
 *
 * @since v0.0.1
 */
public interface ClusteredJobDao {
    /**
     * Returns the next run time for the given job ID, if there is one.
     * <p>
     * Implementations <strong>MAY</strong> implement this with:
     * </p>
     * <pre><code>
     *     ClusteredJob clusteredJob = find(jobId);
     *     return (clusteredJob != null) ? clusteredJob.getNextRunTime() : null;
     * </code></pre>
     * <p>
     * However, this would be wasteful, and implementations <strong>SHOULD</strong> use a more
     * efficient query when possible.
     * </p>
     *
     * @param jobId the job ID whose existence and current version is to be checked
     * @return the currently existing next run time for that {@code jobId}; {@code null} if no such job exists
     * or if it exists but its schedule prevents it from ever running again.
     */
    @Nullable
    Date getNextRunTime(JobId jobId);

    /**
     * Returns the current version of the persisted clustered job with this ID.
     * <p>
     * Implementations <strong>MAY</strong> implement this with:
     * </p>
     * <pre><code>
     *     ClusteredJob clusteredJob = find(jobId);
     *     return (clusteredJob != null) ? clusteredJob.getVersion() : null;
     * </code></pre>
     * <p>
     * However, this would be wasteful, and implementations <strong>SHOULD</strong> use a more
     * efficient query when possible.
     * </p>
     *
     * @param jobId the job ID whose existence and current version is to be checked
     * @return the currently existing version for that {@code jobId}, or {@code null} if no such job exists
     */
    @Nullable
    Long getVersion(JobId jobId);

    /**
     * Finds and returns the persisted details for a clustered job.
     *
     * @param jobId the job ID for the job details to be loaded
     * @return the existing job details, or {@code null} if the job does not exist
     */
    @Nullable
    ClusteredJob find(JobId jobId);

    /**
     * Returns all jobs that use the given job runner.
     * <p>
     * Implementations:
     * </p>
     * <ul>
     * <li><strong>SHOULD</strong> issue a query of the form {@code SELECT * FROM jobs WHERE jobRunnerKey = ?}
     * to retrieve all job details that match the given {@code jobRunnerKey}.</li>
     * <li><strong>MUST</strong> return a {@code ClusteredJob} for each clustered job that has been persisted
     * with the specified {@code jobRunnerKey}.</li>
     * </ul>
     *
     * @param jobRunnerKey the job runner key for which to search
     * @return the set of clustered jobs with the given job runner, in no particular order
     */
    @Nonnull
    Collection<ClusteredJob> findByJobRunnerKey(JobRunnerKey jobRunnerKey);

    /**
     * Returns all jobs that use the given job runners.
     * <p>
     * Implementations:
     * </p>
     * <ul>
     * <li><strong>SHOULD</strong></li> issue a query in the form {@code SELECT * FROM jobs WHERE jobRunnerKey IN (?,?,?...)}.
     * Oracle has a limit of 1000 elements on IN statement,
     * hence if there are more than 1000 elements in the passed list, the query should be split into batches.
     * <li><strong>MUST</strong> return all {@code ClusteredJob} that has been persisted with job runner keys in {@code List<JobRunnerKey>}.</li>
     * </ul>
     * Default implementation iterates through the provided list, calls {@link #findByJobRunnerKey(JobRunnerKey)} for each element, and
     * gathers results in a set.
     * @param jobRunnerKeys list of job runner keys passed to IN statement in query
     * @return the set of clustered jobs with the given job runners, in no particular order
     */
    @Nonnull
    default Collection<ClusteredJob> findByJobRunnerKeys(List<JobRunnerKey> jobRunnerKeys) {
        Set<ClusteredJob> clusteredJobs = new HashSet<>();
        for (JobRunnerKey jobRunnerKey : jobRunnerKeys) {
            clusteredJobs.addAll(findByJobRunnerKey(jobRunnerKey));
        }
        return clusteredJobs;
    }

    /**
     * Returns the job ID and next run time for every clustered job that should be in the queue.
     * <p>
     * Implementations:
     * </p>
     * <ul>
     * <li><strong>SHOULD</strong> issue a query of the form
     * {@code SELECT jobId, nextRunTime FROM jobs WHERE nextRunTime IS NOT NULL} to retrieve all
     * existing job IDs that have scheduled next time to run.</li>
     * <li><strong>MUST</strong> return a mapping of all job IDs to the next run time for that job ID.
     * The keys and values <strong>MUST NOT</strong> be {@code null} for any entry in the
     * returned map.</li>
     * </ul>
     */
    @Nonnull
    Map<JobId, Date> refresh();

    /**
     * Returns the set of distinct job runner keys that have at least one job schedule stored here.
     * <p>
     * Implementations:
     * </p>
     * <ul>
     * <li><strong>SHOULD</strong> issue a query of the form {@code SELECT DISTINCT jobRunnerKey FROM jobs}
     * to retrieve all unique job runner keys.</li>
     * <li><strong>MUST</strong> return the canonical set of job runner keys, regardless of whether
     * or not they have any jobs that will ever run again &mdash; that is, it does not matter
     * whether the {@code nextRunTime} is {@code null} or not.</li>
     * </ul>
     *
     * @return the canonical set of all job runner keys that have at least one scheduled job, whether it
     * will ever run again or not
     */
    @Nonnull
    Set<JobRunnerKey> findAllJobRunnerKeys();

    /**
     * Called to create a new clustered job in the database.
     * <p>
     * Implementations:
     * </p>
     * <ul>
     * <li><strong>SHOULD</strong> issue a statement of the form {@code INSERT INTO jobs ... VALUES ...}
     * to create a representation of this job.</li>
     * <li><strong>MUST</strong> return {@code false} rather than throw an exception if the request fails
     * due to a {@code UNIQUE} constraint</li>
     * <li><strong>MAY</strong> attempt to determine such failures with a {@code SELECT} for a conflicting
     * entry if the persistence layer does not make it possible to distinguish {@code UNIQUE} constraint
     * failures reliably.</li>
     * </ul>
     *
     * @param clusteredJob the job details to be created
     */
    boolean create(ClusteredJob clusteredJob);

    /**
     * Updates the next run time for an existing clustered job.
     * <p>
     * Implementations:
     * </p>
     * <ul>
     * <li><strong>SHOULD</strong> issue a statement of the form
     * {@code UPDATE jobs SET nextRunTime = ?, version = ? WHERE jobId = ? AND version = ?}</li>
     * <li><strong>MUST</strong> ensure that the update occurs if and only if the job is already persisted with
     * the expected version.</li>
     * <li><strong>MUST</strong> change the version as part of the update</li>
     * <li><strong>MUST</strong> return {@code false} if no rows are updated, whether because the job does not
     * exist or because the specified version is incorrect.</li>
     * </ul>
     *
     * @param jobId           the job ID for which to update the next run time
     * @param nextRunTime     the new next run time, or {@code null} if it will never run again
     * @param expectedVersion the version of the job details that are expected to be found
     * @return {@code true} if the update is successful; {@code false} otherwise
     */
    boolean updateNextRunTime(JobId jobId, @Nullable Date nextRunTime, long expectedVersion);

    /**
     * Called to remove a scheduled cluster job by its ID.
     * <p>
     * Implementations:
     * </p>
     * <ul>
     * <li><strong>SHOULD</strong> issue a statement of the form {@code DELETE FROM jobs WHERE jobId = ?}</li>
     * <li><strong>MUST</strong> return {@code false} if there is no matching row to delete; note that this
     * is not an error and the implementation <strong>MUST NOT</strong> throw an exception for it</li>
     * </ul>
     *
     * @param jobId the job to be unscheduled
     * @return {@code true} if the job is successfully removed; {@code false}, otherwise
     */
    boolean delete(JobId jobId);
}
