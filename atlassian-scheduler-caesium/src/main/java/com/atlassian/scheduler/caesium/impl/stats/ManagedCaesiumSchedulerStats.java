package com.atlassian.scheduler.caesium.impl.stats;

import java.util.concurrent.atomic.AtomicLong;

import com.atlassian.jira.util.stats.ManagedStats;
import com.atlassian.jira.util.stats.MutableLongStats;
import com.atlassian.jira.util.stats.TopNSerializableStatsWithFrequencies;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobId;

import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.SECONDS;

/*
{
  "_statsName": "SchedulerStats",
  "_statsType": "total",
  "_time": "2023-09-01T09:40:58.862Z",
  "_timestamp": 1693561258862,
  "_duration": "PT39H9M9.899S",
  "_invocations": 102110,
  "_statsOverhead": "n/a",
  "jobFlow": {
    "takenFromQueue": 17516,
    "localBegin": 6840,
    "localStartedTooEarly": 0,
    "localPreEnqueue": 6840,
    "localFailedSchedulingNextRun": 0,
    "localPreLaunch": 6840,
    "localPostLaunch": 6840,
    "clusteredBegin": 10675,
    "clusteredSkipNoLongerExists": 0,
    "clusteredSkipTooEarly": 0,
    "clusteredSkipFailedToClaim": 0,
    "clusteredPreEnqueue": 10674,
    "clusteredPreLaunch": 10674,
    "clusteredPostLaunch": 10674
  },
  "jobRunnerExecutions": {
    "successful": {
      "jobRunTimeMillis": {
        "count": 14536,
        "min": 0,
        "max": 58273,
        "sum": 169537,
        "avg": 12,
        "distributionCounter": {
          "100": 14434,
          "1000": 87,
          "10000": 13,
          "60000": 1,
          "600000": 1,
          "3600000" : 0,
          "36000000" : 0,
          "9223372036854775807": 0
        }
      },
      "jobIds": {
        "AnalyticsJob": 1081,
        "com.atlassian.jira.service.JiraService:10422": 1081,
        "com.atlassian.jira.service.JiraService:10000": 1080,
        "LastAccessedTimeBatcherJob": 966,
        "class com.atlassian.scheduler.core.util.JobRunnerRegistry:com.atlassian.cluster.monitoring.cluster-m... @2100337330": 966,
        "com.atlassian.jira.plugin.cluster.monitoring.internal.job.ApplicationStatusCollector": 966,
        "com.atlassian.plugins.authentication.impl.basicauth.job.UpdateBasicAuthConfigJob:job": 966,
        "SettingsReloaderJob": 965,
        "com.atlassian.jira.plugins.inform.batching.cron.BatchNotificationJobSchedulerImpl": 965,
        "com.codebarrel.jira.plugin.automation.schedule.RuleInsightsUpdateScheduler.MINUTE.job": 965
      }
    },
    "failed": {
      "jobRunTimeMillis": {
        "count": 0,
        "min": 0,
        "max": 0,
        "sum": 0,
        "avg": 0,
        "distributionCounter": {
          "100": 0,
          "1000": 0,
          "10000": 0,
          "60000": 0,
          "600000": 0,
          "3600000" : 0,
          "36000000" : 0,
        }
      },
      "jobIds": {},
      "reasons": {}
    }
  },
  "localRetryJob": {
    "scheduleCount": 0,
    "scheduleReasons": {},
    "fatalSerializationErrorCount": 0,
    "fatalSerializationErrors": {},
    "fatalSchedulingErrorCount": 0,
    "fatalSchedulingErrors": {}
  },
  "clusterRecoveryJob": {
    "scheduledSuccessfully": {
      "count": 0,
      "reasons": {}
    },
    "schedulingFailed": {
      "count": 0,
      "reasons": {}
    },
    "completedSuccessfully": {
      "runNumber": {
        "count": 0,
        "min": 0,
        "max": 0,
        "sum": 0,
        "avg": 0,
        "distributionCounter": {
          "1": 0,
          "2": 0,
          "5": 0
        }
      }
    }
  },
  "refreshClusterJobsDifference": {
    "count": 1,
    "min": 17,
    "max": 17,
    "sum": 17,
    "avg": 17,
    "distributionCounter": {
      "-100": 0,
      "-10": 0,
      "-1": 0,
      "0": 0,
      "1": 0,
      "10": 0,
      "100": 1
    }
  }
}
 */
interface ManagedCaesiumSchedulerStats extends CaesiumSchedulerStats, ManagedStats {

    class Data implements ManagedCaesiumSchedulerStats {

        final JobFlow jobFlow = new JobFlow();

        final JobRunnerExecutions jobRunnerExecutions = new JobRunnerExecutions();

        final RetryJob localRetryJob = new RetryJob();

        final RecoveryJob clusterRecoveryJob = new RecoveryJob();

        final MutableLongStats refreshClusterJobsDifference = new MutableLongStats(-100, -10, -1, 0, 1, 10, 100);

        static class JobFlow {
            AtomicLong takenFromQueue = new AtomicLong(),
                    localBegin = new AtomicLong(),
                    localStartedTooEarly = new AtomicLong(),
                    localPreEnqueue = new AtomicLong(),
                    localFailedSchedulingNextRun = new AtomicLong(),
                    localPreLaunch = new AtomicLong(),
                    localPostLaunch = new AtomicLong(),
                    clusteredBegin = new AtomicLong(),
                    clusteredSkipNoLongerExists = new AtomicLong(),
                    clusteredSkipTooEarly = new AtomicLong(),
                    clusteredSkipFailedToClaim = new AtomicLong(),
                    clusteredPreEnqueue = new AtomicLong(),
                    clusteredPreLaunch = new AtomicLong(),
                    clusteredPostLaunch = new AtomicLong();
        }

        static class JobRunnerExecutions {
            final Successful successful = new Successful();
            final Failed failed = new Failed();

            static class Successful {
                final MutableLongStats jobRunTimeMillis = newJobRunTimeMillisStats();
                final TopNSerializableStatsWithFrequencies<JobId> jobIds = newJobIdsStats();
            }

            static class Failed {
                final MutableLongStats jobRunTimeMillis = newJobRunTimeMillisStats();
                final TopNSerializableStatsWithFrequencies<JobId> jobIds = newJobIdsStats();
                final TopNSerializableStatsWithFrequencies<String> reasons = newReasonStats();
            }

            private static MutableLongStats newJobRunTimeMillisStats() {
                return new MutableLongStats(
                        100,
                        SECONDS.toMillis(1),
                        SECONDS.toMillis(10),
                        MINUTES.toMillis(1),
                        MINUTES.toMillis(10),
                        HOURS.toMillis(1),
                        HOURS.toMillis(10));
            }

            private static TopNSerializableStatsWithFrequencies<JobId> newJobIdsStats() {
                return new TopNSerializableStatsWithFrequencies<>(500, 10, 100);
            }
        }

        static class RetryJob {

            final AtomicLong scheduleCount = new AtomicLong();
            final TopNSerializableStatsWithFrequencies<String> scheduleReasons = newReasonStats();
            final AtomicLong fatalSerializationErrorCount = new AtomicLong();
            final TopNSerializableStatsWithFrequencies<String> fatalSerializationErrors = newReasonStats();
            final AtomicLong fatalSchedulingErrorCount = new AtomicLong();
            final TopNSerializableStatsWithFrequencies<String> fatalSchedulingErrors = newReasonStats();
        }

        static class RecoveryJob {

            final Scheduled scheduledSuccessfully = new Scheduled();
            final Scheduled schedulingFailed = new Scheduled();
            final CompletedSuccessfully completedSuccessfully = new CompletedSuccessfully();

            static class Scheduled {
                final AtomicLong count = new AtomicLong();
                final TopNSerializableStatsWithFrequencies<String> reasons = newReasonStats();
            }

            static class CompletedSuccessfully {
                final MutableLongStats runNumber = new MutableLongStats(1, 2, 5);
            }
        }

        private static TopNSerializableStatsWithFrequencies<String> newReasonStats() {
            return new TopNSerializableStatsWithFrequencies<>(50, 10, 200);
        }

        @Override
        public String getStatsName() {
            return STATS_NAME;
        }

        @Override
        public void jobFlowTakenFromQueue() {
            jobFlow.takenFromQueue.incrementAndGet();
        }

        @Override
        public void jobFlowLocalBegin() {
            jobFlow.localBegin.incrementAndGet();
        }

        @Override
        public void jobFlowLocalStartedTooEarly() {
            jobFlow.localStartedTooEarly.incrementAndGet();
        }

        @Override
        public void jobFlowLocalPreEnqueue() {
            jobFlow.localPreEnqueue.incrementAndGet();
        }

        @Override
        public void jobFlowLocalFailedSchedulingNextRun() {
            jobFlow.localFailedSchedulingNextRun.incrementAndGet();
        }

        @Override
        public void jobFlowLocalPreLaunch() {
            jobFlow.localPreLaunch.incrementAndGet();
        }

        @Override
        public void jobFlowLocalPostLaunch() {
            jobFlow.localPostLaunch.incrementAndGet();
        }

        @Override
        public void jobFlowClusteredBegin() {
            jobFlow.clusteredBegin.incrementAndGet();
        }

        @Override
        public void jobFlowClusteredSkipNoLongerExists() {
            jobFlow.clusteredSkipNoLongerExists.incrementAndGet();
        }

        @Override
        public void jobFlowClusteredSkipTooEarly() {
            jobFlow.clusteredSkipTooEarly.incrementAndGet();
        }

        @Override
        public void jobFlowClusteredSkipFailedToClaim() {
            jobFlow.clusteredSkipFailedToClaim.incrementAndGet();
        }

        @Override
        public void jobFlowClusteredPreEnqueue() {
            jobFlow.clusteredPreEnqueue.incrementAndGet();
        }

        @Override
        public void jobFlowClusteredPreLaunch() {
            jobFlow.clusteredPreLaunch.incrementAndGet();
        }

        @Override
        public void jobFlowClusteredPostLaunch() {
            jobFlow.clusteredPostLaunch.incrementAndGet();
        }

        @Override
        public void jobRunnerCompletedSuccessfully(final JobId jobId, final long jobRunTimeMillis) {
            jobRunnerExecutions.successful.jobRunTimeMillis.accept(jobRunTimeMillis);
            jobRunnerExecutions.successful.jobIds.store(jobId);
        }

        @Override
        public void jobRunnerFailed(final JobId jobId, final long jobRunTimeMillis, final Throwable t) {
            jobRunnerExecutions.failed.jobRunTimeMillis.accept(jobRunTimeMillis);
            jobRunnerExecutions.failed.jobIds.store(jobId);
            jobRunnerExecutions.failed.reasons.store(getThrowableAsString(t));
        }

        @Override
        public void retryJobScheduled(final Throwable throwableOnSchedulingNextRun) {
            localRetryJob.scheduleCount.incrementAndGet();
            localRetryJob.scheduleReasons.store(getThrowableAsString(throwableOnSchedulingNextRun));
        }

        @Override
        public void retryJobSerializationError(final SchedulerServiceException exceptionOnSerialization) {
            localRetryJob.fatalSerializationErrorCount.incrementAndGet();
            localRetryJob.fatalSerializationErrors.store(getThrowableAsString(exceptionOnSerialization));
        }

        @Override
        public void retryJobScheduleError(final Throwable throwableOnSchedulingRetry) {
            localRetryJob.fatalSchedulingErrorCount.incrementAndGet();
            localRetryJob.fatalSchedulingErrors.store(getThrowableAsString(throwableOnSchedulingRetry));
        }

        @Override
        public void recoveryJobScheduledSuccessfully(final Throwable reason) {
            clusterRecoveryJob.scheduledSuccessfully.count.incrementAndGet();
            clusterRecoveryJob.scheduledSuccessfully.reasons.store(getThrowableAsString(reason));
        }

        @Override
        public void recoveryJobSchedulingFailed(final Throwable throwableOnSchedulingRecoveryJob) {
            clusterRecoveryJob.schedulingFailed.count.incrementAndGet();
            clusterRecoveryJob.schedulingFailed.reasons.store(getThrowableAsString(throwableOnSchedulingRecoveryJob));
        }

        @Override
        public void recoveryJobCompletedSuccessfully(final int runNumber) {
            clusterRecoveryJob.completedSuccessfully.runNumber.accept(runNumber);
        }

        @Override
        public void refreshClusteredJobs(final int pendingJobCountDifference) {
            refreshClusterJobsDifference.accept(pendingJobCountDifference);
        }

        private static String getThrowableAsString(final Throwable reason) {
            final String exceptionClass = reason.getClass().getName();
            final String exceptionMessage = reason.getMessage();
            final StackTraceElement[] stackTrace = reason.getStackTrace();
            final String topOfStack = stackTrace.length > 0 ? stackTrace[0].toString() : "(unknown)";
            return exceptionClass + ": " + exceptionMessage + " at " + topOfStack;
        }
    }
}
