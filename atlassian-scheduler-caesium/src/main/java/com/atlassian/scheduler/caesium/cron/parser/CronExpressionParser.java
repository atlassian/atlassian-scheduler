package com.atlassian.scheduler.caesium.cron.parser;

import java.util.BitSet;
import java.util.Locale;
import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import com.atlassian.scheduler.caesium.cron.parser.CronLexer.Token;
import com.atlassian.scheduler.caesium.cron.rule.CronExpression;
import com.atlassian.scheduler.caesium.cron.rule.field.BitSetFieldRule;
import com.atlassian.scheduler.caesium.cron.rule.field.DayOfWeekFieldRule;
import com.atlassian.scheduler.caesium.cron.rule.field.FieldRule;
import com.atlassian.scheduler.caesium.cron.rule.field.SpecialDayOfMonthFieldRule;
import com.atlassian.scheduler.caesium.cron.rule.field.SpecialDayOfWeekLastFieldRule;
import com.atlassian.scheduler.caesium.cron.rule.field.SpecialDayOfWeekNthFieldRule;
import com.atlassian.scheduler.cron.CronSyntaxException;
import com.atlassian.scheduler.cron.ErrorCode;

import static java.util.Objects.requireNonNull;

import static com.atlassian.scheduler.cron.ErrorCode.COMMA_WITH_LAST_DOM;
import static com.atlassian.scheduler.cron.ErrorCode.COMMA_WITH_LAST_DOW;
import static com.atlassian.scheduler.cron.ErrorCode.COMMA_WITH_NTH_DOW;
import static com.atlassian.scheduler.cron.ErrorCode.COMMA_WITH_WEEKDAY_DOM;
import static com.atlassian.scheduler.cron.ErrorCode.ILLEGAL_CHARACTER;
import static com.atlassian.scheduler.cron.ErrorCode.ILLEGAL_CHARACTER_AFTER_HASH;
import static com.atlassian.scheduler.cron.ErrorCode.ILLEGAL_CHARACTER_AFTER_INTERVAL;
import static com.atlassian.scheduler.cron.ErrorCode.ILLEGAL_CHARACTER_AFTER_QM;
import static com.atlassian.scheduler.cron.ErrorCode.INTERNAL_PARSER_FAILURE;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NAME_RANGE;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NUMBER_DAY_OF_MONTH_OFFSET;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_NUMBER_YEAR_RANGE;
import static com.atlassian.scheduler.cron.ErrorCode.INVALID_STEP;
import static com.atlassian.scheduler.cron.ErrorCode.QM_CANNOT_USE_FOR_BOTH_DAYS;
import static com.atlassian.scheduler.cron.ErrorCode.QM_CANNOT_USE_HERE;
import static com.atlassian.scheduler.cron.ErrorCode.QM_MUST_USE_FOR_ONE_OF_DAYS;
import static com.atlassian.scheduler.cron.ErrorCode.UNEXPECTED_END_OF_EXPRESSION;
import static com.atlassian.scheduler.cron.ErrorCode.UNEXPECTED_TOKEN_FLAG_L;
import static com.atlassian.scheduler.cron.ErrorCode.UNEXPECTED_TOKEN_FLAG_W;

/**
 * Parser for cron expressions.
 * <p>
 * Strictly speaking, "names" are only permitted in the month and day-of-week fields;
 * however, to simplify the parser, they are accepted by the parser and the field type
 * fails to resolve them, instead.
 * </p><p>
 * Tokens are collected by {@link CronLexer}.  Although a few special cases exist to help maintain compatibility
 * with Quartz, the <a href="http://en.wikipedia.org/wiki/Extended_Backus%E2%80%93Naur_Form">EBNF</a> looks
 * something like this:
 * </p>
 *
 * <pre><code>
 *
 * (* ========================================================= *)
 * (*  Basic syntax available to all fields, like 2-7,9 or &#x2A;/3  *)
 * (*  English names and ranges, like MON,WED,FRI or APR-SEP    *)
 * (*  are also included here.                                  *)
 * (* ========================================================= *)
 *
 * slash_interval = SLASH , NUMBER
 *
 * number_range = NUMBER , HYPHEN , NUMBER , [ slash_interval ]
 *
 * name_range = NAME , HYPHEN , NAME , [ slash_interval ]
 *
 * number_expression = number_range , [ slash_interval ]
 *                   | NUMBER , [ slash_interval ]
 *
 * name_expression = name_range
 *                 | NAME , [ slash_interval ]
 *
 * asterisk_expression = ASTERISK , [ slash_interval ]
 *
 * simple_expression = name_expression
 *                   | number_expression
 *                   | asterisk_expression
 *                   | slash_interval  (* implied asterisk *)
 *
 * simple_field = simple_expression , { COMMA , simple_expression }
 *
 * (* ========================================================= *)
 * (*  Special syntax for day-of-month, like 15W or L-3W        *)
 * (* ========================================================= *)
 *
 * special_dom_last_offset_number = NUMBER , [ FLAG_W ]
 *
 * special_dom_last_offset = special_dom_last_offset_number
 *
 * special_dom_last_weekday = FLAG_W
 *
 * special_dom_last = FLAG_L , HYPHEN ,  special_dom_last_offset
 *                  | FLAG_L , special_dom_last_weekday
 *                  | FLAG_L
 *
 * special_dom_weekday = FLAG_W
 *
 * special_dom_field = special_dom_last
 *                   | special_dom_weekday
 *
 * (* ========================================================= *)
 * (*  Special syntax for day-of-week, like 6#3 or 4L           *)
 * (* ========================================================= *)
 *
 * special_dow_nth = HASH , NUMBER
 *
 * special_dow_name = NAME , special_dow_nth
 *
 * special_dow_number = NUMBER , special_dow_nth
 *                    | NUMBER , FLAG_L
 *
 * special_dow_field = special_dow_name
 *                   | special_dow_number
 *                   | FLAG_L   (* Synonymous with "7" when by itself like this *)
 *
 * (* ========================================================= *)
 * (*  Field definitions                                        *)
 * (* ========================================================= *)
 *
 * second_field = simple_field
 *
 * minute_field = simple_field
 *
 * hour_field = simple_field
 *
 * dom_field = special_dom_field
 *           | simple_field
 *
 * month_field = simple_field
 *
 * dow_field = special_dow_field
 *           | simple_field
 *
 * year_field = [ simple_field , [ WHITESPACE , { (* any sequence of unparsed tokens *) } ]
 *
 * (* ========================================================= *)
 * (*  Full expression grammar                                  *)
 * (* ========================================================= *)
 *
 * second_minute_hour = second_field , WHITESPACE, minute_field , WHITESPACE , hour_field , WHITESPACE
 *
 * qm_month_dow = QUESTION_MARK , WHITESPACE , month_field , WHITESPACE , dow_field
 *
 * dom_month_qm = dom_field , WHITESPACE , month_field, WHITESPACE , QUESTION_MARK
 *
 * dom_month_dow = qm_month_dow
 *               | dom_month_qm
 *
 * cron_expression = [ WHITESPACE ] , second_minute_hour , dom_month_dow , [ WHITESPACE , year_field ] ;
 *
 * </code></pre>
 *
 * @since v0.0.1
 */
public class CronExpressionParser {
    /**
     * Parses the supplied cron expression into a form that can evaluate it against candidate dates
     * or find the next time that it matches.
     *
     * @param cronExpression the cron expression to parse
     * @return the parsed form of the cron expression
     * @throws CronSyntaxException if there is a problem with the cron expression
     */
    public static CronExpression parse(final String cronExpression) throws CronSyntaxException {
        return new CronExpressionParser(cronExpression).parseCronExpression();
    }

    /**
     * Returns {@code true} if {@link #parse(String)} will succeed; {@code false} if it will report a syntax error.
     *
     * @param cronExpression the cron expression to parse
     * @return {@code true} if {@link #parse(String)} will succeed; {@code false} if it will report a syntax error.
     */
    public static boolean isValid(final String cronExpression) {
        try {
            parse(cronExpression);
            return true;
        } catch (CronSyntaxException cse) {
            return false;
        }
    }

    private final String cronExpression;
    private final CronLexer lexer;

    private FieldRule secondField;
    private FieldRule minuteField;
    private FieldRule hourField;
    private FieldRule monthField;
    private FieldRule dayField;
    private FieldRule yearField;

    // Per-field state
    private FieldType fieldType;
    private final BitSet values = new BitSet(64);

    private CronExpressionParser(final String cronExpression) {
        // Note: Only ASCII letters are permitted in cron expressions anyway, so the US locale should be fine
        final String toUpper = requireNonNull(cronExpression, "cronExpression").toUpperCase(Locale.US);
        this.cronExpression = toUpper;
        this.lexer = new CronLexer(toUpper);
    }

    private void initField(FieldType fieldType) throws CronSyntaxException {
        this.fieldType = fieldType;
        values.clear();
        assertMoreTokens();
    }

    /*
     * (* ========================================================= *)
     * (*  Basic syntax available to all fields, like 2-7,9 or &#x2A;/3  *)
     * (*  English names and ranges, like MON,WED,FRI or APR-SEP    *)
     * (*  are also included here.                                  *)
     * (* ========================================================= *)
     *
     * slash_interval = SLASH , NUMBER
     *
     * number_range = NUMBER , HYPHEN , NUMBER , [ slash_interval ]
     *
     * name_range = NAME , HYPHEN , NAME , [ slash_interval ]
     *
     * number_expression = number_range , [ slash_interval ]
     *                   | NUMBER , [ slash_interval ]
     *
     * name_expression = name_range
     *                 | NAME , [ slash_interval ]
     *
     * asterisk_expression = ASTERISK , [ slash_interval ]
     *
     * simple_expression = name_expression
     *                   | number_expression
     *                   | asterisk_expression
     *                   | slash_interval  (* implied asterisk *)
     *
     * simple_field = simple_expression , { COMMA , simple_expression }
     */
    private void parseSlashInterval(final int min, final int max) throws CronSyntaxException {
        if (peekType() != TokenType.SLASH) {
            setValues(min, max);
            return;
        }

        skip(TokenType.SLASH);
        final int step = parseStep(lexer.nextToken());
        setValues(min, max, step);

        final Token token = lexer.peekToken();
        switch (token.getType()) {
            case COMMA:
            case NOTHING:
            case WHITESPACE:
                break;
            default:
                throw syntaxError(ILLEGAL_CHARACTER_AFTER_INTERVAL)
                        .errorOffset(token.getStart())
                        .value(token.getChar())
                        .build();
        }
    }

    private void parseNumberRange(final int min) throws CronSyntaxException {
        skip(TokenType.HYPHEN);
        final Token token = lexer.peekToken();
        switch (token.getType()) {
            case NUMBER:
                final int max = parseFieldValue(lexer.nextToken());
                if (fieldType == FieldType.YEAR && max < min) {
                    throw syntaxErrorAt(token, INVALID_NUMBER_YEAR_RANGE);
                }
                parseSlashInterval(min, max);
                return;

            case NAME:
                fieldType.resolveName(token);
                throw syntaxErrorAt(token, INVALID_NAME_RANGE);
        }
        throw token.unexpected();
    }

    private void parseNameRange(final int min) throws CronSyntaxException {
        skip(TokenType.HYPHEN);
        final Token token = lexer.peekToken();
        switch (token.getType()) {
            case NAME:
                final int max = fieldType.resolveName(lexer.nextToken());
                parseSlashInterval(min, max);
                return;

            case NUMBER:
                throw syntaxErrorAt(token, INVALID_NAME_RANGE);
        }
        throw token.unexpected();
    }

    private void parseNumberExpression() throws CronSyntaxException {
        final int min = parseFieldValue(lexer.nextToken());
        if (peekType() == TokenType.HYPHEN) {
            parseNumberRange(min);
        } else {
            parseSlashInterval(min, -1);
        }
    }

    private void parseNameExpression() throws CronSyntaxException {
        final int min = fieldType.resolveName(lexer.nextToken());
        if (peekType() == TokenType.HYPHEN) {
            parseNameRange(min);
        } else {
            parseSlashInterval(min, -1);
        }
    }

    private void parseAsteriskExpression() throws CronSyntaxException {
        skip(TokenType.ASTERISK);
        parseSlashInterval(fieldType.getMinimumValue(), fieldType.getMaximumValue());
    }

    private void parseSimpleExpression() throws CronSyntaxException {
        switch (peekType()) {
            case NUMBER:
                parseNumberExpression();
                break;
            case NAME:
                parseNameExpression();
                break;
            case ASTERISK:
                parseAsteriskExpression();
                break;
            case SLASH:
                parseSlashInterval(fieldType.getMinimumValue(), fieldType.getMaximumValue());
                break;
            case FLAG_L:
                throw unexpectedFlagL(lexer.nextToken());
            case FLAG_W:
                throw syntaxErrorAt(lexer.nextToken(), UNEXPECTED_TOKEN_FLAG_W);
            case QUESTION_MARK:
                throw syntaxErrorAt(lexer.nextToken(), QM_CANNOT_USE_HERE);
            default:
                throw lexer.nextToken().unexpected();
        }
    }

    private void parseSimpleField() throws CronSyntaxException {
        parseSimpleExpression();
        while (peekType() == TokenType.COMMA) {
            skip(TokenType.COMMA);
            parseSimpleExpression();
        }
    }

    @Nonnull
    private FieldRule parseSimpleFieldToRule() throws CronSyntaxException {
        final Token firstToken = lexer.peekToken();
        if (firstToken.getType() == TokenType.ASTERISK) {
            skip(TokenType.ASTERISK);
            if (peekType().isFieldSeparator()) {
                return fieldType.all();
            }
            lexer.moveTo(firstToken);
        }
        parseSimpleField();
        return BitSetFieldRule.of(fieldType.getField(), values);
    }

    /*
     * (* ========================================================= *)
     * (*  Special syntax for day-of-month, like 15W or L-3W        *)
     * (* ========================================================= *)
     *
     * special_dom_last_offset_number = NUMBER , [ FLAG_W ]
     *
     * special_dom_last_offset = special_dom_last_offset_number
     *
     * special_dom_last_weekday = FLAG_W
     *
     * special_dom_last = FLAG_L , HYPHEN , special_dom_last_offset
     *                  | FLAG_L , special_dom_last_weekday
     *                  | FLAG_L
     *
     * special_dom_weekday = FLAG_W
     *
     * special_dom_field = special_dom_last
     *                   | special_dom_weekday
     */
    private FieldRule parseSpecialDomLastOffsetNumber(final int days) throws CronSyntaxException {
        final Token token = lexer.peekToken();
        switch (token.getType()) {
            case FLAG_W:
                skip(TokenType.FLAG_W);
                assertFieldSeparator(COMMA_WITH_LAST_DOM);
                return new SpecialDayOfMonthFieldRule(-days, true);
            case COMMA:
                throw syntaxErrorAt(token, COMMA_WITH_LAST_DOM);
        }
        assertFieldSeparator(COMMA_WITH_LAST_DOM);
        return new SpecialDayOfMonthFieldRule(-days, false);
    }

    private FieldRule parseSpecialDomLastOffset() throws CronSyntaxException {
        skip(TokenType.HYPHEN);

        switch (peekType()) {
            case NUMBER:
                final int days = parseNumber(lexer.nextToken(), INVALID_NUMBER_DAY_OF_MONTH_OFFSET, 1, 30);
                return parseSpecialDomLastOffsetNumber(days);

                // This is a weird one.  In Quartz 1.x, "L-W" is rejected as if it were a name.  This is because it
                // does not support the L-3 or L-2W syntax at all.  In Quartz 2.x, "L-W" is accepted and acts the
                // same as "LW".  Although it doesn't fit the EBNF, it seems harmless enough to warrant tolerating it.
            case FLAG_W:
                return parseSpecialDomLastWeekday();

                // "L-", "L-X" or something like that.  We'll report the error back at the 'L' flag
            default:
                throw syntaxError(UNEXPECTED_TOKEN_FLAG_L)
                        .errorOffset(lexer.peekToken().getStart() - 2)
                        .build();
        }
    }

    private FieldRule parseSpecialDomLastWeekday() throws CronSyntaxException {
        skip(TokenType.FLAG_W);
        assertFieldSeparator(COMMA_WITH_LAST_DOM);
        return new SpecialDayOfMonthFieldRule(0, true);
    }

    @Nullable
    private FieldRule parseSpecialDomLast() throws CronSyntaxException {
        skip(TokenType.FLAG_L);
        final Token token = lexer.peekToken();
        switch (token.getType()) {
            case HYPHEN:
                return parseSpecialDomLastOffset();
            case FLAG_W:
                return parseSpecialDomLastWeekday();
            case WHITESPACE:
                return new SpecialDayOfMonthFieldRule(0, false);
            case COMMA:
                assertFieldSeparator(COMMA_WITH_LAST_DOM);
            case NOTHING:
                throw syntaxError(UNEXPECTED_END_OF_EXPRESSION)
                        .errorOffset(cronExpression.length())
                        .build();
            default:
                throw illegalChar(token);
        }
    }

    @Nullable
    private FieldRule parseSpecialDomWeekday() throws CronSyntaxException {
        final int cronDayOfWeek = parseFieldValue(lexer.nextToken());
        if (lexer.nextToken().getType() == TokenType.FLAG_W) {

            assertFieldSeparator(COMMA_WITH_WEEKDAY_DOM);
            return new SpecialDayOfMonthFieldRule(cronDayOfWeek, true);
        }
        return null;
    }

    @Nullable
    private FieldRule parseSpecialDomField() throws CronSyntaxException {
        switch (peekType()) {
            case FLAG_L:
                return parseSpecialDomLast();
            case NUMBER:
                return parseSpecialDomWeekday();
        }
        return null;
    }

    /*
     * (* ========================================================= *)
     * (*  Special syntax for day-of-week, like 6#3 or 4L           *)
     * (* ========================================================= *)
     *
     * special_dow_nth = HASH , NUMBER
     *
     * special_dow_name = NAME , special_dow_nth
     *
     * special_dow_number = NUMBER , special_dow_nth
     *                    | NUMBER , FLAG_L
     *
     * special_dow_field = special_dow_name
     *                   | special_dow_number
     *                   | FLAG_L   (* Synonymous with "7" when by itself like this *)
     */
    private FieldRule parseSpecialDowNth(final int cronDayOfWeek) throws CronSyntaxException {
        final Token token = lexer.nextToken();
        final int nth = parseNumber(token, ILLEGAL_CHARACTER_AFTER_HASH, 1, 5);
        assertFieldSeparator(COMMA_WITH_NTH_DOW);
        return new SpecialDayOfWeekNthFieldRule(cronDayOfWeek, nth);
    }

    @Nullable
    private FieldRule parseSpecialDowName(final Token name) throws CronSyntaxException {
        final int cronDayOfWeek = fieldType.resolveName(name);
        if (lexer.nextToken().getType() == TokenType.HASH) {
            return parseSpecialDowNth(cronDayOfWeek);
        }
        return null;
    }

    @Nullable
    private FieldRule parseSpecialDowNumber(final Token number) throws CronSyntaxException {
        final int cronDayOfWeek = parseFieldValue(number);
        switch (lexer.nextToken().getType()) {
            case HASH:
                return parseSpecialDowNth(cronDayOfWeek);
            case FLAG_L:
                assertFieldSeparator(COMMA_WITH_LAST_DOW);
                return new SpecialDayOfWeekLastFieldRule(cronDayOfWeek);
            default:
                return null;
        }
    }

    @Nullable
    private FieldRule parseSpecialDowField() throws CronSyntaxException {
        final Token token = lexer.nextToken();
        switch (token.getType()) {
            case NAME:
                return parseSpecialDowName(token);
            case NUMBER:
                return parseSpecialDowNumber(token);
            case FLAG_L:
                assertFieldSeparator(COMMA_WITH_LAST_DOW);
                return DayOfWeekFieldRule.saturday();
            case QUESTION_MARK:
                throw syntaxErrorAt(token, QM_CANNOT_USE_FOR_BOTH_DAYS);
        }
        return null;
    }

    /*
     * (* ========================================================= *)
     * (*  Field definitions                                        *)
     * (* ========================================================= *)
     *
     * second_field = simple_field
     *
     * minute_field = simple_field
     *
     * hour_field = simple_field
     *
     * dom_field = special_dom_field
     *           | simple_field
     *
     * month_field = simple_field
     *
     * dow_field = special_dow_field
     *           | simple_field
     *
     * year_field = [ simple_field , [ WHITESPACE , { (* any sequence of unparsed tokens *) } ]
     */

    private void parseSecondField() throws CronSyntaxException {
        initField(FieldType.SECOND);
        secondField = parseSimpleFieldToRule();
    }

    private void parseMinuteField() throws CronSyntaxException {
        initField(FieldType.MINUTE);
        minuteField = parseSimpleFieldToRule();
    }

    private void parseHourField() throws CronSyntaxException {
        initField(FieldType.HOUR);
        hourField = parseSimpleFieldToRule();
    }

    private void parseDomField() throws CronSyntaxException {
        initField(FieldType.DAY_OF_MONTH);
        final Token mark = lexer.peekToken();

        dayField = parseSpecialDomField();
        if (dayField == null) {
            lexer.moveTo(mark);
            dayField = parseSimpleFieldToRule();
        }
    }

    private void parseMonthField() throws CronSyntaxException {
        initField(FieldType.MONTH);
        monthField = parseSimpleFieldToRule();
    }

    private void parseDowField() throws CronSyntaxException {
        initField(FieldType.DAY_OF_WEEK);
        final Token mark = lexer.peekToken();

        dayField = parseSpecialDowField();
        if (dayField == null) {
            lexer.moveTo(mark);
            parseSimpleField();
            dayField = DayOfWeekFieldRule.of(values);
        }
    }

    private void parseYearField() throws CronSyntaxException {
        yearField = FieldType.YEAR.all();
        if (lexer.hasMoreTokens()) {
            parseWhitespace();
            if (lexer.hasMoreTokens()) {
                initField(FieldType.YEAR);
                yearField = parseSimpleFieldToRule();

                // Make sure that space separates the year from any additional garbage, which we'll ignore
                if (lexer.hasMoreTokens()) {
                    parseWhitespace();
                }
            }
        }
    }

    /*
     * (* ========================================================= *)
     * (*  Full expression grammar                                  *)
     * (* ========================================================= *)
     *
     * second_minute_hour = second_field , WHITESPACE, minute_field , WHITESPACE , hour_field , WHITESPACE
     *
     * qm_month_dow = QUESTION_MARK , WHITESPACE , month_field , WHITESPACE , dow_field
     *
     * dom_month_qm = dom_field , WHITESPACE , month_field, WHITESPACE , QUESTION_MARK
     *
     * dom_month_dow = qm_month_dow
     *               | dom_month_qm
     *
     * cron_expression = [ WHITESPACE ] , second_minute_hour , dom_month_dow , [ WHITESPACE , year_field ] ;
     */
    private void parseSecondMinuteHour() throws CronSyntaxException {
        parseSecondField();
        parseWhitespace();
        parseMinuteField();
        parseWhitespace();
        parseHourField();
        parseWhitespace();
    }

    private void parseQmMonthDow() throws CronSyntaxException {
        parseQuestionMark();
        parseWhitespace();
        parseMonthField();
        parseWhitespace();
        parseDowField();
    }

    private void parseDomMonthQm() throws CronSyntaxException {
        parseDomField();
        parseWhitespace();
        parseMonthField();
        parseWhitespace();
        parseQuestionMark();
    }

    private void parseDomMonthDow() throws CronSyntaxException {
        if (peekType() == TokenType.QUESTION_MARK) {
            parseQmMonthDow();
        } else {
            parseDomMonthQm();
        }
    }

    private CronExpression parseCronExpression() throws CronSyntaxException {
        try {
            if (peekType() == TokenType.WHITESPACE) {
                parseWhitespace();
            }
            parseSecondMinuteHour();
            parseDomMonthDow();
            parseYearField();
            return new CronExpression(
                    cronExpression, yearField, monthField, dayField, hourField, minuteField, secondField);
        } catch (RuntimeException re) {
            throw syntaxError(INTERNAL_PARSER_FAILURE)
                    .cronExpression(cronExpression)
                    .cause(re)
                    .value(re.toString())
                    .build();
        }
    }

    // ================================================================
    //  Parser state assertions and utility methods
    // ================================================================

    private void parseWhitespace() throws CronSyntaxException {
        final Token token = lexer.nextToken();
        switch (token.getType()) {
            case WHITESPACE:
                return;
            case FLAG_L:
                throw unexpectedFlagL(token);
            case FLAG_W:
                throw unexpectedFlagW(token);
            case HASH:
                throw unexpectedHash(token);
            case NOTHING:
                throw syntaxError(UNEXPECTED_END_OF_EXPRESSION)
                        .errorOffset(cronExpression.length())
                        .build();
            default:
                throw token.unexpected();
        }
    }

    private void parseQuestionMark() throws CronSyntaxException {
        final Token token = lexer.nextToken();
        if (token.getType() != TokenType.QUESTION_MARK) {
            throw syntaxError(QM_MUST_USE_FOR_ONE_OF_DAYS)
                    .errorOffset(token.getStart())
                    .build();
        }
        assertNothingAfterQuestionMark();
    }

    private void assertNothingAfterQuestionMark() throws CronSyntaxException {
        final Token token = lexer.peekToken();
        if (!token.getType().isFieldSeparator()) {
            throw syntaxError(ILLEGAL_CHARACTER_AFTER_QM)
                    .errorOffset(token.getStart())
                    .value(token.getChar())
                    .build();
        }
    }

    private void assertFieldSeparator() throws CronSyntaxException {
        final Token token = lexer.peekToken();
        if (!token.getType().isFieldSeparator()) {
            switch (token.getType()) {
                case FLAG_L:
                    throw unexpectedFlagL(token);
                case FLAG_W:
                    throw unexpectedFlagW(token);
                default:
                    throw token.unexpected();
            }
        }
    }

    private void assertFieldSeparator(final ErrorCode errorCodeForComma) throws CronSyntaxException {
        final Token token = lexer.peekToken();
        if (token.getType() == TokenType.COMMA) {
            throw syntaxError(errorCodeForComma).errorOffset(token.getStart()).build();
        }
        assertFieldSeparator();
    }

    private void assertMoreTokens() throws CronSyntaxException {
        if (peekType() == TokenType.NOTHING) {
            throw syntaxError(UNEXPECTED_END_OF_EXPRESSION)
                    .errorOffset(cronExpression.length())
                    .build();
        }
    }

    private void skip(TokenType expected) {
        final Token skipped = lexer.nextToken();
        if (skipped.getType() != expected) {
            throw new IllegalStateException("Expected to skip " + expected + "; found " + skipped);
        }
    }

    private TokenType peekType() {
        return lexer.peekToken().getType();
    }

    private CronSyntaxException.Builder syntaxError(ErrorCode errorCode) {
        return CronSyntaxException.builder().cronExpression(cronExpression).errorCode(errorCode);
    }

    private CronSyntaxException syntaxErrorAt(final Token token, final ErrorCode errorCode) {
        return syntaxError(errorCode).errorOffset(token.getStart()).build();
    }

    private CronSyntaxException illegalChar(final Token token) {
        return syntaxError(ILLEGAL_CHARACTER)
                .errorOffset(token.getStart())
                .value(token.getChar())
                .build();
    }

    private CronSyntaxException unexpectedFlagL(final Token token) {
        switch (fieldType) {
            case DAY_OF_MONTH:
                return syntaxErrorAt(token, COMMA_WITH_LAST_DOM);
            case DAY_OF_WEEK:
                return syntaxErrorAt(token, COMMA_WITH_LAST_DOW);
            default:
                return token.unexpected();
        }
    }

    private CronSyntaxException unexpectedFlagW(final Token token) {
        if (fieldType == FieldType.DAY_OF_MONTH) {
            return syntaxErrorAt(token, COMMA_WITH_WEEKDAY_DOM);
        }
        return token.unexpected();
    }

    private CronSyntaxException unexpectedHash(final Token token) {
        if (fieldType == FieldType.DAY_OF_WEEK) {
            return syntaxErrorAt(token, COMMA_WITH_NTH_DOW);
        }
        return token.unexpected();
    }

    private int parseNumber(final Token token, final ErrorCode errorCode) throws CronSyntaxException {
        try {
            switch (token.getType()) {
                case NOTHING:
                    throw syntaxErrorAt(token, UNEXPECTED_END_OF_EXPRESSION);

                case NUMBER:
                    return Integer.parseInt(token.getText());

                default:
                    throw syntaxErrorAt(token, errorCode);
            }
        } catch (NumberFormatException nfe) {
            throw syntaxError(errorCode)
                    .errorOffset(token.getStart())
                    .cause(nfe)
                    .build();
        }
    }

    private int parseNumber(final Token token, final ErrorCode errorCode, final int min, final int max)
            throws CronSyntaxException {
        final int value = parseNumber(token, errorCode);
        if (value < min || value > max) {
            throw syntaxErrorAt(token, errorCode);
        }
        return value;
    }

    private int parseFieldValue(final Token token) throws CronSyntaxException {
        return parseNumber(
                token, fieldType.getValueErrorCode(), fieldType.getMinimumValue(), fieldType.getMaximumValue());
    }

    private int parseStep(final Token token) throws CronSyntaxException {
        if (token.getType() != TokenType.NUMBER) {
            throw syntaxErrorAt(token, INVALID_STEP);
        }

        final int step = parseNumber(token, fieldType.getStepErrorCode());
        if (step <= 0) {
            throw syntaxErrorAt(token, INVALID_STEP);
        }

        if (fieldType != FieldType.YEAR && step > fieldType.getMaximumValue() - fieldType.getMinimumValue()) {
            throw syntaxError(fieldType.getStepErrorCode())
                    .errorOffset(token.getStart())
                    .value(token.getText())
                    .build();
        }
        return step;
    }

    /**
     * Called to set a single value or range of values when no interval was provided.
     *
     * @param min the left side of the range or the single value
     * @param max the right side of the range or {@code -1} when a single value was given
     */
    private void setValues(final int min, final int max) {
        if (max == -1 || max == min) {
            values.set(min);
        } else if (max > min) {
            // Normal range, such as 4-7
            values.set(min, max + 1);
        } else {
            // Inverted range, such as 7-4 -- independently fill in upper and lower ranges
            values.set(min, fieldType.getMaximumValue() + 1);
            values.set(fieldType.getMinimumValue(), max + 1);
        }
    }

    /**
     * Called to set a range of values when an interval was provided.
     * If {@code -1} is given for max, this means the expression was of the form {@code 3/5}.  In this case,
     * a range is implied with the right side of the range taken to be the maximum legal value for the field.
     *
     * @param min  the left side of the range or the single value
     * @param max  the right side of the range or {@code -1} when a single value was given
     * @param step the step interval
     */
    private void setValues(final int min, final int max, final int step) {
        final int end = (max != -1) ? max : fieldType.getMaximumValue();
        if (step == 1) {
            setValues(min, end);
        } else if (min <= end) {
            // Normal range order such as 4-9/2, which gives (4, 6, 8)
            setValuesRange(min, end, step);
        } else {
            // Inverted range order such as 9-4/2, which gives (9, 11, 1, 3) for months
            final int wrapValue = setValuesRange(min, fieldType.getMaximumValue(), step);
            setValuesRange(wrapValue - fieldType.getWrapOffset(), end, step);
        }
    }

    /**
     * Sets the values beginning at {@code start} and incrementing by {@code step} until past the {@code end}.
     * <p>
     * Examples:
     * </p>
     * <ul>
     * <li>{@code setValuesRange(2, 8, 5)} &mdash; sets the values {@code (2, 7)} and returns {@code 12}</li>
     * <li>{@code setValuesRange(3, 8, 2)} &mdash; sets the values {@code (3, 5, 7)} and returns {@code 9}</li>
     * <li>{@code setValuesRange(3, 9, 2)} &mdash; sets the values {@code (3, 5, 7, 9)} and returns {@code 11}</li>
     * </ul>
     *
     * @param start the first value to set
     * @param end   the value used to determine that the value is past the end of the range.  Must be {@code >= start}.
     * @param step  the step interval for incrementing the value
     * @return the first value reached that was larger than {@code end}.
     */
    private int setValuesRange(final int start, final int end, final int step) {
        int value = start;
        while (value <= end) {
            values.set(value);
            value += step;
        }
        return value;
    }
}
