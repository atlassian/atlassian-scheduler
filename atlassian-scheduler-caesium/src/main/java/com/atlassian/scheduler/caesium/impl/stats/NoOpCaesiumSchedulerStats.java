package com.atlassian.scheduler.caesium.impl.stats;

import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobId;

class NoOpCaesiumSchedulerStats implements CaesiumSchedulerStats {

    @Override
    public void jobFlowTakenFromQueue() {}

    @Override
    public void jobFlowLocalBegin() {}

    @Override
    public void jobFlowLocalStartedTooEarly() {}

    @Override
    public void jobFlowLocalPreEnqueue() {}

    @Override
    public void jobFlowLocalFailedSchedulingNextRun() {}

    @Override
    public void jobFlowLocalPreLaunch() {}

    @Override
    public void jobFlowLocalPostLaunch() {}

    @Override
    public void jobFlowClusteredBegin() {}

    @Override
    public void jobFlowClusteredSkipNoLongerExists() {}

    @Override
    public void jobFlowClusteredSkipTooEarly() {}

    @Override
    public void jobFlowClusteredSkipFailedToClaim() {}

    @Override
    public void jobFlowClusteredPreEnqueue() {}

    @Override
    public void jobFlowClusteredPreLaunch() {}

    @Override
    public void jobFlowClusteredPostLaunch() {}

    @Override
    public void jobRunnerCompletedSuccessfully(final JobId jobId, final long jobRunTimeMillis) {}

    @Override
    public void jobRunnerFailed(final JobId jobId, final long jobRunTimeMillis, final Throwable t) {}

    @Override
    public void retryJobScheduled(final Throwable throwableOnSchedulingNextRun) {}

    @Override
    public void retryJobSerializationError(final SchedulerServiceException exceptionOnSerialization) {}

    @Override
    public void retryJobScheduleError(final Throwable throwableOnSchedulingRetry) {}

    @Override
    public void recoveryJobScheduledSuccessfully(final Throwable reason) {}

    @Override
    public void recoveryJobSchedulingFailed(final Throwable throwableOnSchedulingRecoveryJob) {}

    @Override
    public void recoveryJobCompletedSuccessfully(final int runNumber) {}

    @Override
    public void refreshClusteredJobs(final int pendingJobCountDifference) {}

    @Override
    public void close() {}
}
