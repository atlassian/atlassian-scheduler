package com.atlassian.scheduler.caesium.migration;

/**
 * A stub of one of Quartz's classes, for migration purposes.
 *
 * @since v0.0.3
 */
public class StringKeyDirtyFlagMap extends DirtyFlagMap {
    private static final long serialVersionUID = -9076749120524952280L;
    private boolean allowsTransientData = false;
}
