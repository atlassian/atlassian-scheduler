package com.atlassian.scheduler.caesium.cron.rule.field;

import com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate;
import com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate.Field;

/**
 * A cron field implementation that is based on a single accepted value.
 *
 * @since v0.0.1
 */
public class SingleValueFieldRule extends AbstractFieldRule {
    private static final long serialVersionUID = 4153982970557111861L;

    private final int acceptedValue;

    public SingleValueFieldRule(Field field, int acceptedValue) {
        super(field);
        this.acceptedValue = acceptedValue;
    }

    @Override
    public boolean matches(DateTimeTemplate dateTime) {
        return get(dateTime) == acceptedValue;
    }

    @Override
    public boolean first(DateTimeTemplate dateTime) {
        set(dateTime, acceptedValue);
        return true;
    }

    @Override
    public boolean next(DateTimeTemplate dateTime) {
        if (get(dateTime) >= acceptedValue) {
            return false;
        }
        set(dateTime, acceptedValue);
        return true;
    }

    @Override
    protected void appendTo(StringBuilder sb) {
        sb.append(acceptedValue);
    }
}
