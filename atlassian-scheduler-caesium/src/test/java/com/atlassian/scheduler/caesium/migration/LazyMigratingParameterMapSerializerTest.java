package com.atlassian.scheduler.caesium.migration;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import com.google.common.collect.ImmutableMap;

import com.atlassian.scheduler.core.util.ParameterMapSerializer;

import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertThat;

/**
 * @since v0.0.3
 */
public class LazyMigratingParameterMapSerializerTest {
    private static final String TEST_JOB_DATA =
            "ACED0005737200156F72672E71756172747A2E4A6F62446174614D61709FB083E8BFA9B0CB020000"
                    + "787200266F72672E71756172747A2E7574696C732E537472696E674B65794469727479466C61674D"
                    + "61708208E8C3FBC55D280200015A0013616C6C6F77735472616E7369656E74446174617872001D6F"
                    + "72672E71756172747A2E7574696C732E4469727479466C61674D617013E62EAD28760ACE0200025A"
                    + "000564697274794C00036D617074000F4C6A6176612F7574696C2F4D61703B787001737200116A61"
                    + "76612E7574696C2E486173684D61700507DAC1C31660D103000246000A6C6F6164466163746F7249"
                    + "00097468726573686F6C6478703F4000000000000C7708000000100000000174000A706172616D65"
                    + "74657273707800";

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void testMigrationNotNeeded() throws Exception {
        final Map<String, Serializable> original = ImmutableMap.<String, Serializable>of("hello", "world");
        final ParameterMapSerializer normal = new ParameterMapSerializer();
        final ParameterMapSerializer migrating = new LazyMigratingParameterMapSerializer();

        final byte[] normalBytes = normal.serializeParameters(original);
        final Map<String, Serializable> normalRestored = deserialize(normal, normalBytes);
        assertThat("Normal serializer wasn't reversible?", normalRestored, instanceOf(original.getClass()));

        final byte[] migratingBytes = normal.serializeParameters(original);
        assertArrayEquals("Should produce same serialized form", normalBytes, migratingBytes);
        final Map<String, Serializable> migratingRestored = deserialize(migrating, normalBytes);
        assertThat("This should reproduce the original, too", migratingRestored, instanceOf(original.getClass()));
    }

    @Test
    public void testMigrationNeeded() throws Exception {
        final ParameterMapSerializer parameterMapSerializer = new ParameterMapSerializer();
        expectedException.expect(ClassNotFoundException.class);
        expectedException.expectMessage("org.quartz.JobDataMap");
        deserialize(parameterMapSerializer, getTestJobData());
    }

    @Test
    public void testMigrationPerformed() throws Exception {
        final Map<String, Serializable> map = deserialize(new LazyMigratingParameterMapSerializer(), getTestJobData());
        assertThat(map, instanceOf(ImmutableMap.class));
    }

    private Map<String, Serializable> deserialize(ParameterMapSerializer parameterMapSerializer, byte[] data)
            throws IOException, ClassNotFoundException {
        return parameterMapSerializer.deserializeParameters(getClass().getClassLoader(), data);
    }

    private static byte[] getTestJobData() {
        final String s = TEST_JOB_DATA;
        final byte[] ret = new byte[s.length() / 2];
        for (int i = 0, j = 0; i < ret.length; ++i, j += 2) {
            int x = hex(s.charAt(j)) << 4;
            x += hex(s.charAt(j + 1));
            ret[i] = (byte) x;
        }
        return ret;
    }

    private static int hex(char c) {
        return (c >= 'A') ? (c - 'A' + 10) : (c - '0');
    }
}
