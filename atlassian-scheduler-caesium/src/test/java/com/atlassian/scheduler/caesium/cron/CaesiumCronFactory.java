package com.atlassian.scheduler.caesium.cron;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import javax.annotation.Nullable;

import com.atlassian.scheduler.caesium.cron.parser.CronExpressionParser;
import com.atlassian.scheduler.caesium.cron.rule.CronRule;
import com.atlassian.scheduler.caesium.cron.rule.DateTimeTemplate;
import com.atlassian.scheduler.core.tests.CronFactory;
import com.atlassian.scheduler.cron.CronSyntaxException;

/**
 * An adapter that maps Caesium's cron expression handling onto the core tests' specification.
 *
 * @since v0.0.1
 */
public class CaesiumCronFactory implements CronFactory {
    @Override
    public void parseAndDiscard(String cronExpression) {
        parseInternal(cronExpression);
    }

    @Override
    public CronExpressionAdapter parse(final String cronExpression) {
        return parse(cronExpression, ZoneId.systemDefault());
    }

    @Override
    public CronExpressionAdapter parse(final String cronExpression, final ZoneId zone) {
        final CronRule rule = parseInternal(cronExpression);
        return new CronExpressionAdapter() {
            @Override
            public boolean isSatisfiedBy(Date date) {
                return rule.matches(new DateTimeTemplate(date, zone));
            }

            @Nullable
            @Override
            public Date nextRunTime(Date date) {
                final DateTimeTemplate dateTime = new DateTimeTemplate(date, zone);
                while (rule.next(dateTime)) {
                    final ZonedDateTime result = dateTime.toZonedDateTime();
                    if (result != null) {
                        return new Date(result.toInstant().toEpochMilli());
                    }
                }
                return null;
            }

            @Override
            public String toString() {
                return cronExpression;
            }
        };
    }

    private static CronRule parseInternal(final String cronExpression) {
        try {
            return CronExpressionParser.parse(cronExpression);
        } catch (CronSyntaxException cse) {
            final AssertionError err = new AssertionError("Unable to parse cron expression '" + cronExpression + '\'');
            err.initCause(cse);
            throw err;
        }
    }
}
