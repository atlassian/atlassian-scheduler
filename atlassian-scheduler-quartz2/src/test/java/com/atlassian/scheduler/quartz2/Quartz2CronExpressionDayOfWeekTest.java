package com.atlassian.scheduler.quartz2;

import com.atlassian.scheduler.core.tests.CronExpressionDayOfWeekTest;

/**
 * @since v1.5
 */
public class Quartz2CronExpressionDayOfWeekTest extends CronExpressionDayOfWeekTest {
    public Quartz2CronExpressionDayOfWeekTest() {
        super(new Quartz2CronFactory());
    }
}
