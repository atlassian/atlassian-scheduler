package com.atlassian.scheduler.quartz2;

import com.atlassian.scheduler.core.tests.CronExpressionMinutesTest;

/**
 * @since v1.5
 */
public class Quartz2CronExpressionMinutesTest extends CronExpressionMinutesTest {
    public Quartz2CronExpressionMinutesTest() {
        super(new Quartz2CronFactory());
    }
}
