package com.atlassian.scheduler.quartz2;

import java.text.ParseException;
import java.time.ZoneId;
import java.util.Date;
import java.util.TimeZone;
import javax.annotation.Nullable;

import org.quartz.CronExpression;

import com.atlassian.scheduler.core.tests.CronFactory;

/**
 * @since v1.5
 */
public class Quartz2CronFactory implements CronFactory {
    @Override
    public void parseAndDiscard(String cronExpression) {
        parseInternal(cronExpression);
    }

    @Override
    public CronExpressionAdapter parse(final String cronExpression) {
        return parse(cronExpression, ZoneId.systemDefault());
    }

    @Override
    public CronExpressionAdapter parse(final String cronExpression, final ZoneId zone) {
        final CronExpression parsed = parseInternal(cronExpression);
        parsed.setTimeZone(TimeZone.getTimeZone(zone));

        return new CronExpressionAdapter() {
            @Override
            public boolean isSatisfiedBy(Date date) {
                return parsed.isSatisfiedBy(date);
            }

            @Nullable
            @Override
            public Date nextRunTime(Date prevRunTime) {
                return parsed.getNextValidTimeAfter(prevRunTime);
            }

            @Override
            public String toString() {
                return cronExpression;
            }
        };
    }

    private static CronExpression parseInternal(final String cronExpression) {
        try {
            return new CronExpression(cronExpression);
        } catch (ParseException pe) {
            final AssertionError err = new AssertionError("Unable to parse cron expression '" + cronExpression + '\'');
            err.initCause(pe);
            throw err;
        }
    }
}
