package com.atlassian.scheduler.quartz2;

import org.junit.Before;
import org.junit.Rule;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.core.impl.MemoryRunDetailsDao;
import com.atlassian.scheduler.core.spi.RunDetailsDao;
import com.atlassian.scheduler.core.tests.CalculateNextRunTimeTest;
import com.atlassian.scheduler.quartz2.spi.Quartz2SchedulerConfiguration;
import com.atlassian.tenancy.api.TenantAccessor;

import static org.mockito.Mockito.when;

/**
 * @since v1.6.0
 */
public class Quartz2CalculateNextRunTimeTest extends CalculateNextRunTimeTest {
    @Rule
    public MockitoRule mockitoRule = MockitoJUnit.rule();

    @Mock
    private Quartz2SchedulerConfiguration config;

    @Mock
    private TenantAccessor tenentAccessor;

    private RunDetailsDao runDetailsDao = new MemoryRunDetailsDao();

    @Before
    public void setUp() {
        when(config.getLocalSettings()).thenReturn(Quartz2DefaultSettingsFactory.getDefaultLocalSettings());
        when(config.getClusteredSettings()).thenReturn(Quartz2DefaultSettingsFactory.getDefaultClusteredSettings());
    }

    @Override
    protected SchedulerService getSchedulerService() {
        try {
            return new Quartz2SchedulerService(runDetailsDao, config);
        } catch (SchedulerServiceException sse) {
            throw new AssertionError(sse);
        }
    }
}
