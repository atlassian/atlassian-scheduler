package com.atlassian.scheduler.quartz2;

import org.junit.Ignore;

import com.atlassian.scheduler.core.tests.CronExpressionTimingTest;

/**
 * @since v1.5
 */
@Ignore("Slow; run manually if interested")
public class Quartz2CronExpressionTimingTest extends CronExpressionTimingTest {
    public Quartz2CronExpressionTimingTest() {
        super(new Quartz2CronFactory());
    }

    public static void main(String[] args) {
        new Quartz2CronExpressionTimingTest().test();
    }
}
