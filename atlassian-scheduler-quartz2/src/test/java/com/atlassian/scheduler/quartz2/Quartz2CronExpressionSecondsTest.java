package com.atlassian.scheduler.quartz2;

import com.atlassian.scheduler.core.tests.CronExpressionSecondsTest;

/**
 * @since v1.5
 */
public class Quartz2CronExpressionSecondsTest extends CronExpressionSecondsTest {
    public Quartz2CronExpressionSecondsTest() {
        super(new Quartz2CronFactory());
    }
}
