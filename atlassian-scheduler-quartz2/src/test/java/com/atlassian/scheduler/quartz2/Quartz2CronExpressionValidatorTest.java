package com.atlassian.scheduler.quartz2;

import org.junit.Before;
import org.junit.Test;

import com.atlassian.scheduler.core.tests.QuartzCronExpressionValidatorTest;

import static com.atlassian.scheduler.cron.ErrorCode.INVALID_STEP_SECOND_OR_MINUTE;

/**
 * @since v1.4
 */
public class Quartz2CronExpressionValidatorTest extends QuartzCronExpressionValidatorTest {
    @Before
    public void setUp() {
        validator = new Quartz2CronExpressionValidator();
    }

    @Test
    public void testInvalidExpressionsAllowedInThePast() {
        // This bug was fixed in Quartz 2.3.0 - https://github.com/quartz-scheduler/quartz/issues/58
        assertError(
                "0 0/120 8-16 ? * MON-FRI",
                INVALID_STEP_SECOND_OR_MINUTE,
                "The step interval for second or minute must be less than 60: 120",
                "120",
                4);
    }
}
