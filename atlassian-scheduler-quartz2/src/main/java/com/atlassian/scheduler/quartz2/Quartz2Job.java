package com.atlassian.scheduler.quartz2;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.spi.TriggerFiredBundle;

import com.atlassian.scheduler.config.JobId;
import com.atlassian.scheduler.config.RunMode;
import com.atlassian.scheduler.core.AbstractSchedulerService;
import com.atlassian.scheduler.core.JobLauncher;

/**
 * Quartz 2.x {@code Job} that delegates to {@link JobLauncher}.
 *
 * @since v1.0
 *
 * @deprecated since 3.3.0, all classes aligned with quartz are deprecated in favor of using atlassian-scheduler-caesium
 * as the implementation. This class will be removed in the next major version of scheduler.
 */
@Deprecated
public class Quartz2Job implements Job {
    private final JobLauncher jobLauncher;

    Quartz2Job(
            final AbstractSchedulerService schedulerService,
            final RunMode schedulerRunMode,
            final TriggerFiredBundle bundle) {
        final JobId jobId = JobId.of(bundle.getTrigger().getKey().getName());
        this.jobLauncher = new JobLauncher(schedulerService, schedulerRunMode, bundle.getFireTime(), jobId);
    }

    @Override
    public void execute(final JobExecutionContext context) throws JobExecutionException {
        jobLauncher.launch();
    }

    @Override
    public String toString() {
        return "Quartz2Job[jobLauncher=" + jobLauncher + ']';
    }
}
