package com.atlassian.scheduler.quartz2;

import java.text.ParseException;
import java.util.Locale;

import org.quartz.CronExpression;

import com.atlassian.scheduler.cron.CronExpressionValidator;
import com.atlassian.scheduler.cron.CronSyntaxException;

import static com.atlassian.scheduler.core.util.QuartzParseExceptionMapper.mapException;

/**
 * @since v1.4
 *
 * @deprecated since 3.3.0, all classes aligned with quartz are deprecated in favor of using atlassian-scheduler-caesium
 * as the implementation. This class will be removed in the next major version of scheduler.
 * Use {@code com.atlassian.scheduler.caesium.cron.CaesiumCronExpressionValidator}
 */
@Deprecated
public class Quartz2CronExpressionValidator implements CronExpressionValidator {
    @Override
    public boolean isValid(String cronExpression) {
        return CronExpression.isValidExpression(cronExpression);
    }

    @Override
    public void validate(String cronExpression) throws CronSyntaxException {
        try {
            CronExpression.validateExpression(cronExpression);
        } catch (ParseException pe) {
            throw mapException(cronExpression.toUpperCase(Locale.US), pe);
        }
    }
}
