package com.atlassian.scheduler.quartz2.spi;

import java.util.Properties;
import javax.annotation.Nonnull;

import com.atlassian.scheduler.core.spi.SchedulerServiceConfiguration;
import com.atlassian.scheduler.quartz2.Quartz2DefaultSettingsFactory;

/**
 * Provides custom configuration settings for the Quartz 2.x scheduler service.
 *
 * @since v1.0
 *
 * @deprecated since 3.3.0, all classes aligned with quartz are deprecated in favor of using atlassian-scheduler-caesium
 * as the implementation. This class will be removed in the next major version of scheduler.
 *
 * @see
 * <a href="https://bitbucket.org/atlassian/caesium/src/master/atlassian-scheduler-caesium/src/main/java/com/atlassian/scheduler/caesium/spi/CaesiumSchedulerConfiguration.java">
 *     CaesiumSchedulerConfiguration.java
 * </a>
 */
@Deprecated
public interface Quartz2SchedulerConfiguration extends SchedulerServiceConfiguration {
    /**
     * Returns custom properties for configuring the scheduler that will be used to run
     * local jobs.
     * <p>
     * <strong>WARNING</strong>: Since v1.3, the only property that provides a default
     * value is {@code org.quartz.scheduler.skipUpdateCheck}, which is always forced to
     * {@code true} regardless of the settings returned here.  To get the old defaults,
     * you can use {@link Quartz2DefaultSettingsFactory#getDefaultLocalSettings()}
     * as the starting point.
     * </p>
     */
    @Nonnull
    Properties getLocalSettings();

    /**
     * Returns custom properties for configuring the scheduler that will be used to run
     * clustered jobs.
     * <p>
     * <strong>WARNING</strong>: Since v1.3, the only property that provides a default
     * value is {@code org.quartz.scheduler.skipUpdateCheck}, which is always forced to
     * {@code true} regardless of the settings returned here.  To get the old defaults,
     * you can use {@link Quartz2DefaultSettingsFactory#getDefaultClusteredSettings()}
     * as the starting point.
     * </p>
     */
    @Nonnull
    Properties getClusteredSettings();
}
